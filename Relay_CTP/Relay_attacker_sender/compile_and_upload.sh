#!/bin/bash

PATH_FILE=$EDU_HOC_HOME/config/relay_as_paths.txt
JAR=/home/martin/Desktop/Edu-hoc/JeeTool/JeeTool/dist/JeeTool.jar
CLASS=cz.muni.fi.crocs.EduHoc.Main
PROJECT=/home/martin/git/attacks/Relay/Relay_attacker_sender/
#PROJECT=/home/martin/Desktop/Edu-hoc/src/EepromCheck

echo "$J4" > $PATH_FILE

java -cp $JAR $CLASS -a $PATH_FILE -u $PROJECT

rm $PATH_FILE
