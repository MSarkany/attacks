#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../relay.h"

int node_id;
int parent_id;

byte node_distance = 200;   // distance from base station

bool isBS = false;          // set to true on BS

// buffers for own and received messages
char received_message[MSG_BUF_SIZE] = "";
byte received_message_len;
byte received_message_hdr;
char own_message[MSG_BUF_SIZE];
byte own_message_len;
byte own_message_id_len;
unsigned long message_counter = 0;  // message counter, will be replaced

// copies message to buffer to enable further reception on radio
void copyRF12ToBuffer()
{
    received_message_len = rf12_len;
    received_message_hdr = rf12_hdr;
    memcpy(received_message, rf12_data, received_message_len);    
}

// extracts distance from packet
int extractDistance(char *message)
{
    char *dist_ptr = strchr(message, ':') + 1;
    if(!dist_ptr){
        return 0;
    }
    return atoi(dist_ptr);
}

// routing table establishment phase main function for base station
void routingPhaseBS()
{
    delay(2000);    // to give attacker a reasonable chance

    unsigned long start = millis();

    // broadcast distance 0
    while(millis() <= start + RP_DURATION_LEGIT){
        broadcastDistance(node_id, (node_distance = RELAY_BS_DISTANCE));
        rf12_sendWait(0);
        delay(1000);
    }
}

// receives distance messages until end and sets variables accordingly
void handleDistanceMessages(unsigned long end)
{
    while(waitReceive(end)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();

        byte msg_distance = extractDistance(received_message);
        if(!msg_distance){
            continue;
        }
        if(msg_distance + 1 < node_distance){
            node_distance = msg_distance + 1;
            parent_id = received_message_hdr & 0x1f;
        }
    }
}

// routing table establishment phase main function for non-BS nodes
void routingPhaseCommon()
{
    unsigned long start = millis();
    unsigned long ms;

    while((ms = millis()) <= start + RP_DURATION_LEGIT){
        // handle distance messages for half a second
        handleDistanceMessages(ms + 500);

        // broadcast own distance
        broadcastDistance(node_id, node_distance);
        rf12_sendWait(0);
        
        // if there is time left (should be), continue receiving distance messages
        handleDistanceMessages(ms + 1500);
    }    
}

// sends received message to parent
void forwardMessage()
{
    // don't forward message if it's a distance message
    if(isDistanceMsg(received_message)){
        return;
    }

    // create header
    byte header = createHeader(parent_id, 0, 0);

    // send
    rf12_sendNow(header, received_message, received_message_len);
}

// message exchange phase main function for base station
void mxPhaseBS()
{
    unsigned long start = millis();

    // receiver and process messages until for MXP_DURATION_LEGIT milliseconds
    while(waitReceive(start + MXP_DURATION_LEGIT)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();
        
        Serial.println("Received packet:");
        printPacket(received_message_hdr, received_message_len, received_message);
    }
}

// prepares own message to send
// TODO create reasonable messages, using counter for testing
void prepareOwnMessage()
{
    itoa(message_counter, own_message + own_message_id_len, 16);
    own_message_len = strlen(own_message);
    message_counter++;
}

// sends own message
void sendOwnMessage()
{
    byte header =  createHeader(parent_id, MODE_DST, 0);
    rf12_sendNow(header, own_message, own_message_len);
}

// message exchange phase main function for non-BS nodes
void mxPhaseCommon()
{
    unsigned long start = millis();
    unsigned long iter_ms;

    // work for MXP_DURATION_LEGIT milliseconds
    while((iter_ms = millis()) <= start + MXP_DURATION_LEGIT){
        // receive messages for 1 second
        while(waitReceive(iter_ms + 1000)){
            // ignore distance messages
            if(isDistanceMsg(rf12_data)){
                replyAck();
                rf12_recvDone();
                continue;
            }

            replyAck();
            copyRF12ToBuffer();
            rf12_recvDone();
            
            // forward received message            
            forwardMessage();
        }
        
        // prepare and send own message
        prepareOwnMessage();
        sendOwnMessage();
    }
}

void setup () 
{
    // initialize
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Relay attack =====\nLegit node...");

    node_id = EEPROM.read(NODE_ID_LOCATION);
    parent_id = EEPROM.read(PARENT_ID_LOCATION);

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    // BS has equal own and parrent IDs
    if(parent_id == node_id){
        isBS = true;
    }

    // routing table establishment phase
    if(isBS){
        routingPhaseBS();
    } else {
        routingPhaseCommon();
    }

    // start with different delay
    if(!isBS){
        randomSeed(analogRead(0));
        delay(random(1000));
    }
}

void loop () 
{
    // message exchange phase
    if(isBS){
        mxPhaseBS();
    } else {
        itoa(node_id, own_message,10);
        strcat(own_message, ":");
        own_message_id_len = strlen(own_message);
        mxPhaseCommon();
    }

    // sleep until end of times
    Serial.println("End of scenario, going to sleep.");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode(); 
}
