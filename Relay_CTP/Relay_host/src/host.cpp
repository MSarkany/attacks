#include <iostream>

#include "host.h"
#include "utils.h"

using namespace std;


Host::Host(string dev_path, host_mode mode, long long duration)
{
    m_dev_path = dev_path;
    m_mode = mode;
    m_duration = duration;

    // open file descriptor
    m_dev_fd = open(dev_path.c_str() , O_RDWR | O_NOCTTY);
    if(!m_dev_fd){
        HostException ex("Failed to open serial port");
        throw ex;
    }

    // set parameters
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if(tcgetattr(m_dev_fd, &tty) != 0)    {
        HostException ex("Failed to open serial port");
        throw ex;
    }

    if(cfgetispeed(&tty) != B115200 && !cfsetispeed(&tty, B115200)){
        HostException ex("Failed to set input baud rate");
        throw ex;
    }

    if(cfgetospeed(&tty) != B115200 && !cfsetospeed(&tty, B115200)){
        HostException ex("Failed to set output baud rate");
        throw ex;
    }

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_lflag = 0;
    tty.c_cc[VMIN]  = 1;
    tty.c_cc[VTIME] = 5;
    if (tcsetattr(m_dev_fd, TCSANOW, &tty) != 0)    {
        HostException ex("Failed to set flags for serial port");
        throw ex;
    }

}

Host::~Host()
{
    // close file descriptor
    close(m_dev_fd);
}

int Host::read_line(char *buffer, int max_len)
{
    if(!m_dev_fd || !buffer || max_len < 1){
        return 1;
    }

    char c;
    int i = 0;
    int len;
    while((len = read(m_dev_fd, &c, 1))){
        if(len < 1){
            continue;
        }
        if(c == EOF){
            usleep(500000); // half second
            continue;
        } else if(c == '\n' || c == '\r'){
            return i;
        } else {
            buffer[i] = c;
            i++;
        }

        if(i >= max_len){
            return i;
        }
    }

    return i;
}

int set_socket_timeout(int sock, long long timeout_ms)
{
    long long       rest;
    struct timeval  tv;

    tv.tv_sec = timeout_ms/1000;
    rest = timeout_ms - (tv.tv_sec * 1000);
    tv.tv_usec = rest * 1000;

    return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
}

SenderHost::SenderHost(string dev_path, host_mode mode, long long duration, string ip, int port) : Host(dev_path, mode, duration)
{
    // create socket for sending
    m_sock_fd = socket(AF_INET,SOCK_DGRAM,0);

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    inet_aton(ip.c_str(), &server.sin_addr);

    // connect to receiver
    if(connect(m_sock_fd, (sockaddr*) &server, sizeof(server)) < 0){
        HostException ex("Could not establish a connection");
        throw ex;
    }
}

SenderHost::~SenderHost()
{
    // close UDP socket
    close(m_sock_fd);
}

int SenderHost::do_work()
{
    char    buffer[BUFF_SIZE];
    int     rval;
    long long start = millis();

    buffer[BUFF_SIZE] = 0;

    while(millis() < start + m_duration){
        // read line from device
        int len = read_line(buffer, BUFF_SIZE - 1);
        buffer[len] = 0;
        if(len < 1){
            continue;
        }
        // send line over UDP to receiver
        if((rval = send(m_sock_fd, buffer, len, 0)) == -1){
            cerr << "Sending error: " << rval << endl;
            return 1;
        }
    }

    return 0;
}

ReceiverHost::ReceiverHost(string dev_path, host_mode mode, long long duration, int port) : Host(dev_path, mode, duration)
{
    // open UDP socket
    m_sock_fd = socket(AF_INET,SOCK_DGRAM,0);

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = INADDR_ANY;

    // bind socket for reception
    if (bind(m_sock_fd, (struct sockaddr *) &server, sizeof(server)) < 0){
        HostException ex("Failed to bind socket");
        throw ex;
    }
}

ReceiverHost::~ReceiverHost()
{
    // close UDP socket
    close(m_sock_fd);
}

int ReceiverHost::do_work()
{
    char        buffer[BUFF_SIZE];
    int         len;
    long long   start = millis();

    while(millis() < start + m_duration){
        // receive line
        len = recv(m_sock_fd, buffer, BUFF_SIZE, 0);
        if(len < 0){
            return 1;
        }

        buffer[len] = '\n';
        buffer[++len] = 0;

        // === edit packet here ===
        strcat(buffer, "_hacked_");
        len += strlen("_hacked_");
        // ========================

        cout << "Received over UDP: " << buffer << "length: " << len << endl;
        cout.flush();

        // send to device
        int ret = write(m_dev_fd, buffer, len);
        if(ret != len){
            if(ret < 0){
                cerr << "Failed to write to serial port" << endl;
                return 2;
            } else {
                cerr << "Only " << ret << " out of " << len << " bytes were written to serial port" << endl;
            }
        }
    }

    return 0;
}
