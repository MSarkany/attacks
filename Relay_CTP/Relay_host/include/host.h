#ifndef HOST_H
#define HOST_H

#include <iostream>
#include <string>
#include <exception>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#define BUFF_SIZE 2048

using namespace std;

typedef enum { HOST_SENDER = 0, HOST_RECEIVER } host_mode;

// generic pc host for relay attack - both sender and receiver inherit from it
class Host {
    string      m_dev_path; // path to device
    host_mode   m_mode;     // mode - sender or receiver

protected:
    int         m_dev_fd = 0;       // device file descriptor
    int         m_sock_fd = 0;      // UDP socket file descriptor
    long long   m_duration;         // duration of the attack

    // reads line from device
    int read_line(char *buffer, int max_len);

public:
    // constructor
    Host(string dev_path, host_mode mode, long long duration);
    // destructor
    virtual ~Host();
    // function with main loop - handling all the work
    virtual int do_work() = 0;
};


// sender host - sending to receiver host - there may be multiple of these
class SenderHost : public Host {
public:
    SenderHost(string dev_path, host_mode mode, long long duration, string uri, int port);
    virtual ~SenderHost();
    virtual int do_work();

};


// receiver host - receiving from sender
class ReceiverHost : public Host {
public:
    ReceiverHost(string dev_path, host_mode mode, long long duration, int port);
    virtual ~ReceiverHost();
    virtual int do_work();

};


// exception for host constructor
class HostException : public exception {
    string m_message;
public:
    HostException(string message)
    {
        m_message = message;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
};

#endif // HOST_H
