#ifndef _RELAY_H_
#define _RELAY_H_

#include "../common.h"

// phases' durations
#define RP_DURATION_LEGIT       10000
#define RP_DURATION_ATTACKER    10000
#define MXP_DURATION_LEGIT      20000
#define MXP_DURATION_ATTACKER   25000

// IDs
#define RELAY_ATTACKER_ID       17
#define RELAY_BS_ID             1

// testing
#define RELAY_BS_DISTANCE       5   // for testing purposes with 4 nodes only

char distance_message[MSG_BUF_SIZE + 1] = "distance:";
byte distance_message_len;

// broadcasts it's distance from BS
void broadcastDistance(byte node_id, byte distance)
{
    byte header = createHeader(0, MODE_SRC, 0);
    itoa(distance, distance_message + 9, 10);
    rf12_sendNow(header, distance_message, strlen(distance_message));
}

// true if it is a distance message
bool inline isDistanceMsg(const char *msg)
{
    if(msg && msg[0] == 'd'){   // advanced techniques
        return true;    
    }

    return false;
}


#endif // _RELAY_H_
