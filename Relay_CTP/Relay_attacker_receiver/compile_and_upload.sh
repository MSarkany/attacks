#!/bin/bash

PATH_FILE=$EDU_HOC_HOME/config/relay_ar_paths.txt
JAR=/home/martin/Desktop/Edu-hoc/JeeTool/JeeTool/dist/JeeTool.jar
CLASS=cz.muni.fi.crocs.EduHoc.Main
PROJECT=/home/martin/git/attacks/Relay/Relay_attacker_receiver/
#PROJECT=/home/martin/Desktop/Edu-hoc/src/EepromCheck

echo "$J3" > $PATH_FILE

java -cp $JAR $CLASS -a $PATH_FILE -u $PROJECT

rm $PATH_FILE

