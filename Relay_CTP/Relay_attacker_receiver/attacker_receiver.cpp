#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../relay.h"

#define SYBIL_BS_ID 1   // TODO move to header

byte node_id;

byte message_len;
byte message_hdr;
char message[MSG_BUF_SIZE + 1];

String message_from_udp;


// +++++++++++++++++++++++++++ ROUTING PHASE ++++++++++++++++++++++++++

// this node doesn't perform any routing, always sending to BS
void routingPhaseAttackerReceiver()
{
    delay(RP_DURATION_ATTACKER);
}

// +++++++++++++++++++++++ MESSAGE EXCHANGE PHASE +++++++++++++++++++++

// basically reads a line from serial port
bool receiveMessageOoB()
{
    message_from_udp = Serial.readStringUntil('\n');

    return true;
    
}

#ifdef OOBAND
bool msgIsMarked()
{
    // there is exactly 1 more byte after string terminating byte
    return strlen(message) + 2 == message_len;
}

char getMark()
{
    return message[message_len + 1];
}

#endif // OOBAND

// message exchange phase main function
void mxPhaseAttackerReceiver(unsigned long end)
{
#ifdef OOBAND
    bool received;

    // runs until end
    while(millis() < end){
        // receive message from serial port
        received = receiveMessageOoB();

        // send packet to BS
        if(received){
            message_from_udp.toCharArray(message, MSG_BUF_SIZE);
            message_hdr = createHeader(RELAY_BS_ID, MODE_DST, 0);
            rf12_sendNow(message_hdr, message, message_from_udp.length());
        }
    }
#else // OOBAND
    unsigned long start = millis();

    // receive messages for 1 second
    while(waitReceive(start + MXP_DURATION_ATTACKER)){
        // ignore distance messages
        if(isDistanceMsg(rf12_data)){
            replyAck();
            rf12_recvDone();
            continue;
        }

        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();
        
        // forward received message            
        // forwardMessage();
        if(msgIsMarked){
            // char mark = getMark();
            // maybe do something with the value
            message[message_len] = 'X';
            message_len++;
        }

        // create header
        byte header = createHeader(SYBIL_BS_ID, 0, 0);


        // send to BS
        rf12_sendNow(header, received_message, received_message_len);
    }

#endif // OOBAND
}

// ++++++++++++++++++++++++++++++ GENERAL ++++++++++++++++++++++++++++++

void setup () {
    // initialization
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Relay attack =====\n");
    Serial.flush();

    node_id = RELAY_ATTACKER_ID;

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
    
    // timeout for serial port when reading string
    Serial.setTimeout(30000);

    // routing phase main function - just waits
    routingPhaseAttackerReceiver();

    Serial.flush(); 
}

// using infinite loops inside the loop so conditions don't have to be checked all the time
void loop () {
    unsigned long start = millis();

    // message exchange main function - reads messages from serial port and sends them all to BS
    mxPhaseAttackerReceiver(start + MXP_DURATION_ATTACKER);
    
    // end of the scenario - sleeping forever    
    Serial.println("End of the attack, going to sleep");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode();
}
