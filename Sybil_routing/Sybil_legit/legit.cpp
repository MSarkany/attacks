#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../sybil.h"


#define PARENT_NUM      3
#define RANDOM_PARENT

#define IS_BS (node_id == 1)        // node with ID 1 is a BS

int node_id         = 0;            // node ID
int parents_ids[PARENT_NUM];        // parents' IDs
int parents_distances[PARENT_NUM];  // parents' distances
int actual_par_num  = 0;            // actual number of parents
int cur_forw_par    = 0;            // current parent index for msg forwarding
int lowest_distance = 250;          // shortest path distance

// buffers for own and received messages
char received_message[MSG_BUF_SIZE] = "";
byte received_message_len;
byte received_message_hdr;
char own_message[MSG_BUF_SIZE] = "";
byte own_message_len;
byte own_message_id_len;

char distance_message[MSG_BUF_SIZE + 1] = "distance:";
byte distance_message_len;

unsigned long message_counter = 0;  // message counter


// copies message to buffer to enable further reception on radio
void copyRF12ToBuffer()
{
    memset(received_message, 0, MSG_BUF_SIZE);
    received_message_len = rf12_len;
    received_message_hdr = rf12_hdr;
    memcpy(received_message, rf12_data, received_message_len);    
}

// broadcasts it's distance from BS
void broadcastDistance(byte node_id, byte distance){
    byte header = createHeader(node_id, MODE_SRC, 0);
    itoa(distance, distance_message + 9, 10);
    rf12_sendNow(header, distance_message, strlen(distance_message));
}

// routing table establishment phase main function for base station
void routingPhaseBS()
{
    Serial.println("I am BS");
    Serial.flush();

    delay(1000);    // to give attacker a reasonable chance

    unsigned long start = millis();

    // broadcast distance 0
    while(millis() <= start + RP_DURATION_LEGIT){
        broadcastDistance(node_id, SYBIL_BS_DISTANCE);
        rf12_sendWait(0);
        delay(1000);
    }
}

void updateDistances(byte id, byte distance)
{
    // loop through all parents
    for(int i=0;i<PARENT_NUM;i++){
        if(parents_ids[i] == id){
            break;
        }
        // find first parent with higher distance
        if(distance < parents_distances[i]){
            // update
            parents_ids[i] = id;
            parents_distances[i] = distance;

            if(i >= actual_par_num){
                actual_par_num = i + 1;
            }

            if(distance < lowest_distance){
                lowest_distance = distance;
            }
            break; // do not update the rest
        }
    }
}

// extracts distance from packet
int extractDistance(char *message)
{
    char *dist_ptr = strchr(message, ':') + 1;
    if(!dist_ptr){
        return 0xFF;
    }
    return atoi(dist_ptr);
}

void printDebugInfo()
{
    Serial.print("Actual parents: ");
    Serial.println(actual_par_num, DEC);
    for(int i=0;i<PARENT_NUM;i++){
        Serial.print(" - ");
        Serial.print(parents_ids[i], DEC);
        Serial.print(": ");
        Serial.println(parents_distances[i], DEC);
    }
    Serial.print("Lowest distance: ");
    Serial.println(lowest_distance, DEC);
    Serial.print("Current parent: ");
    Serial.println(cur_forw_par, DEC);
    Serial.println();
}

// receives distance messages until end and sets variables accordingly
void handleDistanceMessages(unsigned long end)
{
    while(waitReceive(end)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();        
        
        byte msg_distance = extractDistance(received_message);
        if(msg_distance == 0xFF){
            continue;
        }
        updateDistances(received_message_hdr & 0x1f, msg_distance + 1);
    }
}

// routing table establishment phase main function for non-BS nodes
void routingPhaseCommon()
{
    unsigned long start = millis();
    unsigned long ms;

    while((ms = millis()) <= start + RP_DURATION_LEGIT){
        // handle distance messages for half a second
        handleDistanceMessages(ms + 500);

        // broadcast own distance
        broadcastDistance(node_id, lowest_distance);
        rf12_sendWait(0);
        
        // if there is time left (should be), continue receiving distance messages
        handleDistanceMessages(ms + 1500);
    }    
}

void sendOwnMessage()
{
#ifdef RANDOM_PARENT
    byte parent_id = parents_ids[random(actual_par_num)];
#else // RANDOM_PARENT
    // use parent ID of a next parent
    byte parent_id = parents_ids[cur_forw_par];
    cur_forw_par++;
    cur_forw_par %= actual_par_num;
#endif // RANDOM_PARENT
    
    byte header =  createHeader(parent_id, MODE_DST, 0);
    rf12_sendNow(header, own_message, own_message_len);
    // Serial.print("Sent:      ");
    // printMessage(own_message, own_message_len);
}

// message exchange phase main function for base station
void mxPhaseBS()
{
    unsigned long start = millis();

    // receiver and process messages until for MXP_DURATION_LEGIT milliseconds
    while(waitReceive(start + MXP_DURATION_LEGIT)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();
        
        if(received_message[0] != 'd'){ // no distance packets
            Serial.println("Received packet:");
            printPacket(received_message_hdr, received_message_len, received_message);
        }
    }
}

// true if it is a distance message
bool inline isDistanceMsg(const char *msg)
{
    if(msg && msg[0] == 'd'){   // advanced techniques
        return true;    
    }

    return false;
}

// sends received message to parent
void forwardMessage()
{
    // don't forward message if it's a distance message
    if(isDistanceMsg(received_message)){
        return;
    }

#ifdef RANDOM_PARENT
    byte parent_id = parents_ids[random(actual_par_num)];
#else // RANDOM_PARENT
    // use parent ID of a next parent
    byte parent_id = parents_ids[cur_forw_par];
    cur_forw_par++;
    cur_forw_par %= actual_par_num;
#endif // RANDOM_PARENT

    // create header
    byte header = createHeader(parent_id, MODE_DST, 0);

    Serial.print("Forwarding \"");
    Serial.print(received_message);
    Serial.print("\" to ");
    Serial.println(parent_id);
    Serial.flush();

    // send
    rf12_sendNow(header, received_message, received_message_len);
}

// prepares own message to send
void prepareOwnMessage()
{
    itoa(message_counter, own_message + own_message_id_len, 16);
    own_message_len = strlen(own_message);
    message_counter++;
}

// message exchange phase main function for non-BS nodes
void mxPhaseCommon()
{
    unsigned long start = millis();
    unsigned long iter_ms;

    // work for MXP_DURATION_LEGIT milliseconds
    while((iter_ms = millis()) <= start + MXP_DURATION_LEGIT){
        // receive messages for 1 second
        while(waitReceive(iter_ms + 1000)){
            // ignore distance messages
            if(isDistanceMsg(rf12_data)){
                replyAck();
                rf12_recvDone();
                continue;
            }

            replyAck();
            copyRF12ToBuffer();
            rf12_recvDone();
            
            // forward received message            
            forwardMessage();
        }
        
        // prepare and send own message
        prepareOwnMessage();
        sendOwnMessage();
    }
}


void setup () {
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Sybil attack =====\nLegit node...");

    node_id = EEPROM.read(NODE_ID_LOCATION);

    // prepare own message if not a BS
    if(!IS_BS){
        // init distances
        for(int i=0;i<PARENT_NUM;i++){
            parents_ids[i] = 0; // try to send to BS if no messages arrived
            parents_distances[i] = 250;
        }

        itoa(node_id, own_message, 10);
        strcat(own_message + 1, " says hello");
        own_message_len = strlen(own_message);
        // own_message[own_message_len] = 0;
        randomSeed(analogRead(0));
        delay(random(1000));

    }

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    // routing table establishment phase
    if(IS_BS){
        routingPhaseBS();
    } else {
        routingPhaseCommon();
    }


}

void loop () {
    // message exchange phase
    if(IS_BS){
        mxPhaseBS();
    } else {
        itoa(node_id, own_message,10);
        strcat(own_message, ":");
        own_message_id_len = strlen(own_message);
        mxPhaseCommon();
    }

    // sleep until end of times
    Serial.println("End of scenario, going to sleep.");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode(); 
}
