#ifndef SYBIL__H
#define SYBIL__H

#include "JeeLib.h"
#include "../common.h"

// scenario duration
#define RP_DURATION_LEGIT       15000
#define RP_DURATION_ATTACKER    15000
#define MXP_DURATION_LEGIT      20000
#define MXP_DURATION_ATTACKER   25000

// BS distance
#define SYBIL_BS_DISTANCE   0

// radio settings
#define BAUD_RATE           115200
#define RADIO_FREQ          RF12_868MHZ
#define RADIO_GROUP         10

// EEPROM settings
#define NODE_ID_LOCATION    0
#define GROUP_ID_LOCATION   1
#define PARENT_ID_LOCATION  2

// timing settings
#define DELAY               3000
#define DETECTION_TIME      3000

// buffer size settings
#define MSG_BUF_SIZE        40
#define MAX_DETECTED_NODES  30 // there can be ID different from RF12 ID assigned to each node => more than 30

// devices' IDs settings
#define BS_ID               1
#define ATTACKER_ID         4
#define USE_KNOWN_IDS       false
#define USE_UNKNOWN_IDS     true
#define RANDOMIZE           true

// traffic settings
#define FALSE_TRAFFIC       true
#define MODE_DIRECT         false

// create_header() and send_sybil_message() modes
#define MODE_SRC            0
#define MODE_DST            1

// add_id() return values
#define ID_ADDED            1
#define ID_PRESENT          2
#define ID_BUFF_FULL        3


#endif // SYBIL_H

