#!/bin/bash

JAR=$EDU_HOC_HOME/JeeTool/JeeTool/dist/JeeTool.jar
CLASS=cz.muni.fi.crocs.EduHoc.Main
PROJECT=/home/martin/git/attacks/Sybil_rr/Sybil_attacker/
PATH_FILE=$PROJECT/sybil_attacker_paths.txt

echo "$J6" > $PATH_FILE

java -cp $JAR $CLASS -a $PATH_FILE -u $PROJECT

rm $PATH_FILE

