#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"

#include "../sybil.h"

int node_id = 1;
int parent_id = 0;

unsigned char packet_len;
unsigned char packet_hdr;
unsigned char packet_grp;
unsigned char packet_data[MSG_BUF_SIZE];

String message =  "";
char payload[MSG_BUF_SIZE] = "";

void setup () 
{
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Sybil attack =====\nListening...");

    node_id = EEPROM.read(NODE_ID_LOCATION);
    parent_id = EEPROM.read(PARENT_ID_LOCATION);
   
    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);    
}

void loop () 
{
    if(rf12_recvDone() && rf12_crc == 0){
        replyAck();

        if((packet_len = rf12_len) >= MSG_BUF_SIZE){   // shouldn't happen
            rf12_recvDone();
            return; // back to the beginning of the loop()
        }

        // save values
        packet_hdr = rf12_hdr;
        packet_grp = rf12_grp;
        memcpy(packet_data, rf12_data, packet_len);
        
        rf12_recvDone();    // unlock for next reception

        printPacket(packet_hdr, packet_len, packet_data);
        
        Serial.flush(); 
    }
}

