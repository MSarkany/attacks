#!/bin/bash

PATH_FILE=$EDU_HOC_HOME/config/sybil_receiver.txt
JAR=$EDU_HOC_HOME/JeeTool/JeeTool/dist/JeeTool.jar
CLASS=cz.muni.fi.crocs.EduHoc.Main
PROJECT=/$EDU_HOC_HOME/src/Attacks/Sybil/Sybil_receiver

echo "$J1" > $PATH_FILE

java -cp $JAR $CLASS -a $PATH_FILE -u $PROJECT

rm $PATH_FILE

