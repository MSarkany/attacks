#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"

#include "../sybil.h"

int node_id;
int parent_id;

char received_message[MSG_BUF_SIZE] = "";
byte received_message_len;
char own_message[MSG_BUF_SIZE];
byte own_message_len;

unsigned long round_end;


// performs routing until 'end'
void handleRouting(unsigned long end)
{
    while(waitReceive(end)){
        replyAck();

        received_message_len = rf12_len;
        memcpy(received_message, rf12_data, received_message_len);

        rf12_recvDone();

        byte header = createHeader(parent_id, MODE_DST, 0);
        rf12_sendNow(header, received_message, received_message_len);

        Serial.print("Forwarded: ");
        printMessage(received_message, received_message_len);
    }
}

void sendOwnMessage()
{
    byte header =  createHeader(parent_id, MODE_DST, 0);
    rf12_sendNow(header, own_message, own_message_len);
    Serial.print("Sent:      ");
    printMessage(own_message, own_message_len);
}

void setup () {
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Sybil attack =====\nLegit node...");

    node_id = EEPROM.read(NODE_ID_LOCATION);
    parent_id = EEPROM.read(PARENT_ID_LOCATION);

    itoa(node_id, own_message, 10);
    strcat(own_message + 1, " votes for #3");
    own_message_len = strlen(own_message);
    own_message[own_message_len] = 0;

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
    randomSeed(analogRead(0));

    delay(random(2000));
}


void loop () {
    unsigned long round_start = millis();

    // route some messages but make sure there is time left for sending
    handleRouting(round_start + DELAY / 2);

    // send own stuff
    sendOwnMessage();
    
    // keep routing messages until round's end
    handleRouting(round_start + DELAY);

    Serial.flush(); 
}

