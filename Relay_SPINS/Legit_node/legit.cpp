#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../relay.h"
#include "../../uTESLA_AES/client/uTESLAClient.h"
#include "../../common.h"

#define BUFFER_SIZE 80

static uint16_t node_id             = 0;
static uint16_t parent_id           = 0;
static uint8_t  header              = 0;

static bool     is_BS               = false;          // set to true on BS

// buffers for own and received messages
static uint8_t  rcvd_buff[MSG_BUF_SIZE];
static uint8_t  rcvd_len            = 0;
static uint8_t  rcvd_hdr            = 0;
static char     own_message[MSG_BUF_SIZE];
static uint8_t  own_message_len     = 0;
static const uint8_t last_hash[]    = { 0x30, 0x38, 0x6b, 0x81, 0x72, 0xc5, 0x63, 0x16, 0x32, 0x7,
                                        0xa8, 0xbd, 0xa5, 0x29, 0x6f, 0xf4, 0x2c, 0x24, 0x4a, 0xcf,
                                        0x63, 0x81, 0xb5, 0x50, 0x74, 0x7b, 0x46, 0xc9 };

// static BlakeHMAC       hmac;
// static Blake224        hash;
static AES              aes;
static AESMAC           hmac(&aes);
static AEShash          hash(&aes);
static uTeslaClient     utesla_client(last_hash, &hash, &hmac);


void forwardBeacon()
{
    Serial.println("Fwd:");
    printBuffer(rcvd_buff, rcvd_len);
    header = createHeader(node_id, MODE_SRC, 0);
    delay(node_id * random(1000));
    rf12_sendNow(header, rcvd_buff, rcvd_len);
}

// routing table establishment phase main function for base station
void routingPhaseBS()
{
    uint32_t end = millis() + RP_DURATION_LEGIT;
    char buffer[BUFFER_SIZE];

    while(millis() < end){
        while(Serial.available() < 1 && millis() < end);
        uint8_t len1 = Serial.read();
        while(Serial.available() < 1 && millis() < end);
        uint8_t len2 = Serial.read();
        if(len1 != len2){
            //Incorrect length byte
            Serial.print("LEN");
            Serial.println(len2);
            Serial.flush();
            continue;
        }

        while(Serial.available() < 1 && millis() < end);
        if(Serial.readBytes(buffer, len1) != len1){
            // Failed to receive whole packet from serial port
            Serial.println("BUFF");
            Serial.flush();
            continue;
        }

        Serial.println("OK");
        // printBuffer((uint8_t*)buffer, len1);
        Serial.flush();

        rf12_sendNow(header, buffer, len1);
        return;
    }
}

static bool handleBeacons(unsigned long end)
{
    while(waitReceive(end)){
        copy_rf12_to_buffer;    // fills rcvd_* variables
        if(rcvd_hdr & RF12_HDR_DST){
            continue;
        }
        if(rcvd_buff[0] != MSG_TYPE_KEY)
        if(!utesla_client.updateKey(rcvd_buff + 1)){
            continue;
        }

        parent_id = rcvd_hdr & RF12_HDR_MASK;
        forwardBeacon();
        return true;
    }
    return false;
}

// routing table establishment phase main function for non-BS nodes
static void routingPhaseCommon()
{
    uint32_t end = millis() + RP_DURATION_LEGIT;

    if(!handleBeacons(end)){
        Serial.println("No beacon");
        Serial.flush();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable(); 
        sleep_mode(); 
    }
}

// message exchange phase main function for base station
void mxPhaseBS()
{
    unsigned long start = millis();

    // receiver and process messages until for MXP_DURATION_LEGIT milliseconds
    while(waitReceive(start + MXP_DURATION_LEGIT)){
        copy_rf12_to_buffer;
        
        if(rcvd_buff[0] == MSG_TYPE_DATA){
            printPacket(rcvd_hdr, rcvd_len - 1, rcvd_buff + 1);
        }
    }
}

// message exchange phase main function for non-BS nodes
void mxPhaseCommon()
{
    unsigned long start = millis();
    unsigned long iter_ms;

    // work for MXP_DURATION_LEGIT milliseconds
    while((iter_ms = millis()) <= start + MXP_DURATION_LEGIT){
        // receive messages for 1 second
        while(waitReceive(iter_ms + 1000)){
            copy_rf12_to_buffer;
            if(parent_id == 0 && (!(rcvd_hdr & RF12_HDR_DST)) && rcvd_buff[0] != MSG_TYPE_KEY){
                // forward received message
                // Serial.println("Fwd");
                // printBuffer(rcvd_buff, rcvd_len);  // TODO REMOVE
                rf12_sendNow(header, rcvd_buff, rcvd_len);
            }
        }
        
        // send own message
        // Serial.println("Own");
        // printBuffer(own_message, own_message_len);  // TODO REMOVE
        rf12_sendNow(header, own_message, own_message_len);

        delay(1000);
    }
}

void setup () 
{
    // initialize
    Serial.begin(BAUD_RATE);
    Serial.println("\n===== Relay attack =====\nLegit node...");

    node_id = EEPROM.read(NODE_ID_LOCATION);
    // parent_id = EEPROM.read(PARENT_ID_LOCATION);

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    // BS has equal own and parrent IDs
    if(node_id == 1){
        Serial.println("BS");
        is_BS = true;
    }

    // routing table establishment phase
    if(is_BS){
        routingPhaseBS();
    } else {
        routingPhaseCommon();
    }

    if(!is_BS){
        randomSeed(analogRead(0));
        // delay(random(1000));
    }
}

void loop () 
{
    // message exchange phase
    if(is_BS){
        mxPhaseBS();
    } else {
        memset(own_message, 0, MSG_BUF_SIZE);
        own_message[0] = MSG_TYPE_DATA;
        itoa(node_id, own_message + 1, 10);
        strcat(own_message + 1, "->");
        itoa(parent_id, own_message + strlen(own_message + 1) + 1, 10);
        own_message_len = strlen(own_message + 1) + 1;
        header = createHeader(parent_id, MODE_DST, 0);
        mxPhaseCommon();
    }

    // sleep until end of times
    Serial.println("End of scenario, going to sleep.");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode(); 
}
