#!/bin/bash

while getopts "kch" opt; do
  case $opt in
    k)
        KILL=true
        ;;
    c)
        COMPILE=true
        ;;
    h)
        echo "usage: run_scenario.sh [ -k ] [ -c ]"
        echo "    -k    kill tail and relay_host"
        echo "    -c    compile"
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
  esac
done

if [ "$KILL" = "true" ]; then
    echo "Killing relay_host and tail..."
    killall relay_host tail
fi

if [ "$COMPILE" = "true" ]; then
    echo "Compiling host"
    make -C Relay_host

    echo "Compiling, uploading and running attacker - sender"
    ./Relay_attacker_sender/compile_and_upload.sh
    sleep 1

    echo "Compiling, uploading and running attacker - receiver"
    ./Relay_attacker_receiver/compile_and_upload.sh
    sleep 1

    echo "Compiling and uploading legit nodes"
    ./Relay_legit/compile_and_upload.sh

    sleep 3
fi

echo "Relay attack scenario starting.."

./Relay_host/relay_host -j $J3 -m receiver -p 33431 -d 40000 > receiver_output.txt &
./Relay_host/relay_host -j $J4 -m sender -p 33431 -i 192.168.1.13 -d 40000  &
tail -f < $J2 > jl2_out.txt &
tail -f < $J1

