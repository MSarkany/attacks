#ifndef UTILS_H
#define UTILS_H

// print IPs of network interfaces
int printIPs();

// get ms time
long long millis();

#endif // UTILS_H
