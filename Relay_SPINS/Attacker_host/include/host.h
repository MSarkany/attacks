#ifndef HOST_H
#define HOST_H

#include <iostream>
#include <string>
#include <exception>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#ifndef LINUX_ONLY
#define LINUX_ONLY
#endif
#include <uTESLA.h>

#define BUFF_SIZE       2048
#define RECEIVE_PORT    33333
#define SEND_PORT       3333

using namespace std;

typedef enum { HOST_SENDER = 0, HOST_RECEIVER } host_mode;

// generic pc host for relay attack
class AttackerHost {
private:
    string      m_dev_path; // path to device
    host_mode   m_mode;     // mode - sender or receiver
    int         m_dev_fd;   // device file descriptor
    int         m_sock_fd;  // UDP socket file descriptor
    // int         m_rcvsock_fd;   // TODO make cleaner
    struct sockaddr_in m_addr_other; // other party's address
    long long   m_duration; // duration of the attack

    void modifyPacket(uint8_t *buffer, int *size);

public:
    // constructor
    // Host(string dev_path, host_mode mode, long long duration);
    AttackerHost(const string &dev_path, const host_mode mode, const int64_t duration, const string &ip);
    // destructor
    virtual ~AttackerHost();
    // function with main loop - handling all the work
    virtual int runScenario();
};


// sender host - sending to receiver host - there may be multiple of these
// class SenderHost : public Host {
// public:
//     SenderHost(string dev_path, host_mode mode, long long duration, string uri, int port);
//     virtual ~SenderHost();
//     virtual int do_work();

// };


// // receiver host - receiving from sender
// class ReceiverHost : public Host {
// public:
//     ReceiverHost(string dev_path, host_mode mode, long long duration, int port);
//     virtual ~ReceiverHost();
//     virtual int do_work();

// };


// exception for host constructor
class HostException : public exception {
    string m_message;
public:
    HostException(string message)
    {
        m_message = message;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
};

#endif // HOST_H
