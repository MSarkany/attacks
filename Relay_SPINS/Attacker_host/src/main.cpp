// TODO add signal for duration + signal handler for SIGINT etc.

#include <getopt.h>

#include "host.h"
#include "utils.h"

using namespace std;

void print_usage()
{
    cout << "relay_host [ -i ] [ -p ] [ -d ] [ -m ] [ -j ] " << endl;
    cout << "    -i    IP of the receiver" << endl;
    cout << "    -p    port of the receiver" << endl;
    cout << "    -d    duration" << endl;
    cout << "    -m    mode" << endl;
    cout << "    -j    JeeLink device" << endl;
}

// negative values are errors, positive are converted values
long long string_to_unsigned(const char *str)
{
    if(!str){
        return -1;
    }

    if(str[0] > '9' || str[0] < '0'){
        return -2;
    }

    return atoi(str);
}

int main(int argc, char **argv)
{
    string      dev_path;
    string      mode;
    string      ip;
    // int         port;
    long long   duration;
    host_mode   hmode;
    char        opt;

    // print IPS to see if correctly set
    cout << "IPs:" << endl;
    printIPs();

    // while((opt = getopt(argc, argv, "i:p:d:m:j:")) != -1)
    while((opt = getopt(argc, argv, "i:d:m:j:")) != -1)
    {
        switch(opt)
        {
        case 'i':
            ip = optarg;
            break;
        // case 'p':
        //     port = string_to_unsigned(optarg);
        //     break;
        case 'd':
            duration = string_to_unsigned(optarg);
            break;
        case 'm':
            mode = optarg;
            break;
        case 'j':
            dev_path = optarg;
            break;
        default:
            print_usage();
            break;
        }
    }

    // check args
    if(mode.empty()){
        cerr << "Mode not set" << endl;
        return -1;
    }

    if(mode == "receiver" || mode == "rec"){
        hmode = HOST_RECEIVER;
    } else if(mode == "sender" || mode == "send"){
        hmode = HOST_SENDER;
    } else {
        cerr << "Wrong mode specified" << endl;
        return -2;
    }

    if(ip.empty() && hmode == HOST_SENDER){
        cerr << "Destination IP not set" << endl;
        return -3;
    }

    // if(port < 1 && hmode == HOST_SENDER){
    //     cerr << "Destination port not set or invalid" << endl;
    //     return -4;
    // }

    if(duration < 1){
        cerr << "Duration not set or invalid" << endl;
        return -5;
    }

    if(dev_path.empty()){
        cerr << "Device path not set" << endl;
        return -6;
    }



    // create host
    AttackerHost *host;
    try{
        // host = new AttackerHost(dev_path, hmode, duration, ip, port);
        host = new AttackerHost(dev_path, hmode, duration, ip);
    } catch (HostException &ex){
        cerr << ex.what() << endl;
        return -1;
    }

    // main loop
    host->runScenario();

    delete host;

    return 0;
}
