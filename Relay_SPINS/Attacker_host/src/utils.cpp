#include <ifaddrs.h>
#include <sys/timeb.h>
#include <arpa/inet.h>
#include <cstdio>

// stolen from stackoverflow
int printIPs()
{
    struct ifaddrs  *ifAddrStruct=NULL;
    struct ifaddrs  *ifa=NULL;
    void            *tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) {
            // is IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
        }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);
    return 0;
}

long long millis(){
    timeb tb;
    ftime(&tb);
    return tb.millitm + (tb.time * 1000);
}
