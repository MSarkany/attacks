TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/host.cpp \
    src/main.cpp \
    src/utils.cpp

HEADERS += \
    include/host.h \
    include/utils.h

INCLUDEPATH += \
    include

DISTFILES += \
    Makefile
