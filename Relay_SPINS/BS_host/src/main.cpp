#include <getopt.h>
#include <iostream>
#include <thread>
#include <chrono>

#include "RelayBS.h"

#define DEFAULT_DURATION    20000
#define UTESLA_ROUNDS_NUM   4

using namespace std;


static const uint8_t initial_key[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                        0, 0, 0, 0, 0, 0, 0, 0 };


void print_usage()
{
    cout << "relay_host [ -p /path/to/device] [ -d duration ]" << endl;
}

// negative values are errors, positive are converted values
long long string_to_unsigned(const char *str)
{
    if(!str){
        return -1;
    }

    if(str[0] > '9' || str[0] < '0'){
        return -2;
    }

    return atoi(str);
}

int main(int argc, char **argv)
{
    char        opt;
    string      dev_path    = "/dev/ttyUSB0";
    long long   duration    = DEFAULT_DURATION;


    while((opt = getopt(argc, argv, "i:p:d:m:j:")) != -1)
    {
        switch(opt)
        {
        // case 'i':
        //     ip = optarg;
        //     break;
        // case 'p':
        //     port = string_to_unsigned(optarg);
        //     break;
        case 'd':
            duration = string_to_unsigned(optarg);
            break;
        // case 'm':
        //     mode = optarg;
        //     break;
        // case 'j':
        //     dev_path = optarg;
        //     break;
        case 'p':
            dev_path = optarg;
            break;
        default:
            print_usage();
            break;
        }
    }

    if(duration < 1){
        cout << "Invalid duration, using 20 000 ms" << endl;
        duration = DEFAULT_DURATION;
    }


    // create host
    RelayBS *bs;
    try{
        bs = new RelayBS(dev_path, duration, initial_key, UTESLA_ROUNDS_NUM);
    } catch (RelayBSException ex){
        cerr << "Failed to initialize base station" << endl;
        cerr << ex.what() << endl;
        return -1;
    }

    cout << "Last hash chain element: ";
    cout << "Base station using device " << dev_path << endl;

    cout << "Waiting for node to initialize.." << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(4000));
    cout << "Sending key.." << endl;
    // main loop
    bs->runScenario();

    cout << "End of scenario (BS)" << endl;

    delete bs;

    return 0;
}
