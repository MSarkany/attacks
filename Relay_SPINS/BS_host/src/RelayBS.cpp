#include <iostream>

#include <string.h>

#include "RelayBS.h"
#include "utils.h"

using namespace std;


RelayBS::RelayBS(const string dev_path, const  uint64_t duration, const uint8_t *utesla_initial_key, const uint16_t rounds_num): 
m_hash(&m_aes), m_mac(&m_aes)
{
    m_dev_path = dev_path;
    m_duration = duration;

    // open file descriptor
    m_dev_fd = open(dev_path.c_str() , O_RDWR | O_NOCTTY);
    if(!m_dev_fd){
        RelayBSException ex("Failed to open serial port");
        throw ex;
    }

    // set parameters
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if(tcgetattr(m_dev_fd, &tty) != 0)    {
        RelayBSException ex("Failed to open serial port");
        throw ex;
    }

    if(cfgetispeed(&tty) != B115200 && !cfsetispeed(&tty, B115200)){
        RelayBSException ex("Failed to set input baud rate");
        throw ex;
    }

    if(cfgetospeed(&tty) != B115200 && !cfsetospeed(&tty, B115200)){
        RelayBSException ex("Failed to set output baud rate");
        throw ex;
    }

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_lflag = 0;
    tty.c_cc[VMIN]  = 1;
    tty.c_cc[VTIME] = 5;
    if (tcsetattr(m_dev_fd, TCSANOW, &tty) != 0)    {
        RelayBSException ex("Failed to set flags for serial port");
        throw ex;
    }

    try {
        m_utesla = new uTeslaMaster(m_dev_fd, utesla_initial_key, rounds_num, &m_hash, &m_mac);
    } catch(uTeslaMasterException ex){
        RelayBSException rex(ex.what());
        throw ex;
    }

    cout << "Last hash chain element: " << endl;
    m_utesla->printLastElementHex();
}

RelayBS::~RelayBS()
{
    // close file descriptor
    close(m_dev_fd);
}

bool RelayBS::runScenario()
{
    if(!m_utesla->newRound()){
        cerr << "Failed to broadcast key" << endl;
        return false;
    }

    char    buffer[BUFF_SIZE];
    long long start = millis();

    buffer[BUFF_SIZE] = 0;

    while(millis() < start + m_duration){
        // read line from device
        int len = read_line(buffer, BUFF_SIZE - 1);
        buffer[len] = 0;
        if(len < 1){
            continue;
        }
        
        cout << "Received: " << endl << "  ";
        // printBufferHex((uint8_t*) buffer, len);
        cout << buffer << endl;
        
    }

    return true;
}

int RelayBS::read_line(char *buffer, int max_len)
{
    if(!m_dev_fd || !buffer || max_len < 1){
        return 1;
    }

    char c;
    int i = 0;
    int len;
    while((len = read(m_dev_fd, &c, 1))){
        if(len < 1){
            continue;
        }
        if(c == EOF){
            usleep(500000); // half second
            continue;
        } else if(c == '\n' || c == '\r'){
            return i;
        } else {
            buffer[i] = c;
            i++;
        }

        if(i >= max_len){
            return i;
        }
    }

    return i;
}
