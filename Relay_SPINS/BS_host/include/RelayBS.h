#ifndef HOST_H
#define HOST_H

#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdint.h>

#include "AES_crypto.h"
#include <uTESLAMaster.h>

#define BUFF_SIZE 2048

using namespace std;

class RelayBS {
private:
    // Blake224        m_hash;
    // BlakeHMAC       m_mac;
    AES             m_aes;
    AEShash         m_hash;
    AESMAC          m_mac;
    uTeslaMaster    *m_utesla;
    string          m_dev_path;         // path to device
    int             m_dev_fd    = 0;   // device file descriptor
    long long       m_duration;         // duration of the attack

    // reads line from device
    int read_line(char *buffer, int max_len);

public:
    // constructor
    RelayBS(const string dev_path, const uint64_t duration, const uint8_t *utesla_initial_key, const uint16_t rounds_num);
    // destructor
    virtual ~RelayBS();
    // function with main loop - handling all the work
    bool runScenario();
};


// exception for host constructor
class RelayBSException : public exception {
    string m_message;
public:
    RelayBSException(string message)
    {
        m_message = message;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
};

#endif // HOST_H
