#ifndef RC2CRYPTO_H
#define RC2CRYPTO_H

#include <stdint.h>

#ifndef KEY_SIZE
#define KEY_SIZE        1
#endif

#include "uTESLA.h"
#include "RC2.h"


class RC2hash: public Hash {
private:
    uint8_t m_key[RC2_BLOCK_SIZE] = { 0x00 };
    RC2     *m_rc2;
    uint8_t *m_exp; //expanded key

    uint8_t hashDataBlockB(const uint8_t* buffer, uint8_t offset, uint8_t* key, uint8_t* hash);
    uint8_t hashDataB(const uint8_t* buffer, uint8_t offset, uint8_t len, uint8_t* hash);
public:
    RC2hash(RC2 *rc2);
    virtual bool hash(const uint8_t * const input, const uint16_t input_size, uint8_t *output, const uint16_t output_buufer_size);
    virtual uint8_t hashSize();
};

class RC2MAC: public MAC {
private:
    RC2 *m_rc2;
    uint8_t *m_exp; //expanded key

    uint8_t macBuffer(const uint8_t* key, const uint8_t* buffer, uint8_t offset, uint8_t* pLen, uint8_t* mac);
public:
    RC2MAC(RC2 *rc2);
    virtual bool computeMAC(const uint8_t *key, const uint8_t key_size, const uint8_t *input, const uint16_t input_size, uint8_t *output, const uint16_t output_buffer_size);
    virtual uint8_t keySize();
    virtual uint8_t macSize();
};


#endif //  RC2CRYPTO_H
