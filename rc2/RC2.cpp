
/*
* Modified by Martin Sarkany, 2018
*/

#include <string.h>

#include "RC2.h"

// #define RC2_MODE_ENCRYPT    0
// #define RC2_MODE_DECRYPT    1


void RC2::keyExpansion(uint8_t *expkey, const uint8_t *key)
{
    // no need for key expansion when the whole point is to make it weak
    // hence just copying the key
    memcpy(expkey, key, RC2_KEY_SIZE);
}
 


bool RC2::encrypt(const uint8_t *in_block, uint8_t *expkey, uint8_t *out_block)
{
    if(!in_block || !expkey || !out_block){
        return false;
    }

    RC2_KEY key;

    RC2_set_key(&key, RC2_KEY_SIZE, expkey, RC2_KEY_SIZE * 8);
    RC2_encryptc(in_block, out_block, &key);

    return true;
}

bool RC2::decrypt(const uint8_t *in_block, uint8_t *expkey, uint8_t *out_block)
{
    if(!in_block || !expkey || !out_block){
        return false;
    }

    RC2_KEY key;

    RC2_set_key(&key, RC2_KEY_SIZE, expkey, RC2_KEY_SIZE * 8);
    RC2_decryptc(in_block, out_block, &key);

    return true;
}

