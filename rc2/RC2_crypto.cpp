#include "RC2_crypto.h"

// include for mem*() functions
// #ifdef LINUX_ONLY
#include <string.h>
// #else
// #include <Arduino.h>
// #endif

#ifdef DEBUG
#include <iostream>
#endif

#ifndef NULL
#define NULL ((void*) 0)
#endif

#define BLOCK_SIZE  RC2_BLOCK_SIZE
#define HASH_LENGTH RC2_HASH_SIZE

// global variable shared between rc2-based classes to save some space
uint8_t expanded_key[240]; //expanded key

#ifdef DEBUG
// TODO should not be here
void printBufferHex(const uint8_t *buffer, const int32_t len)
{
    for(int i=0;i<len;i++){
        printf("%02X ", buffer[i]);
    }
    printf("\n");
}
#endif

RC2hash::RC2hash(RC2 *rc2): m_rc2(rc2), m_exp(expanded_key) { }

// uint8_t hashDataBlockB( uint8_t* buffer, uint8_t offset, PL_key_t* key, uint8_t* hash){
uint8_t RC2hash::hashDataBlockB(const uint8_t* buffer, uint8_t offset, uint8_t* key, uint8_t* hash)
{
    uint8_t status = SUCCESS;       
    uint8_t i;
    
    // pl_log_d( TAG,"  hashDataBlockB called.\n");

    // if (verifyArgumentsShort("hashDataBlockB", key, buffer, offset, 1) == FAIL) {
    //     return FAIL;
    // }

    if(hash == NULL){
        // pl_log_e( TAG," hashDataBlockB NULL hash.\n");
        return FAIL;        
    }
    
    m_rc2->keyExpansion( m_exp, key);
    m_rc2->encrypt(buffer + offset, m_exp, hash);
    for(i = 0; i < BLOCK_SIZE; i++){
        hash[i] = buffer[i + offset] ^ hash[i];
    }       
    return status;
}

uint8_t RC2hash::hashDataB(const uint8_t* buffer, uint8_t offset, uint8_t len, uint8_t* hash)
{
    uint8_t status = SUCCESS;
    uint8_t i;
    uint8_t j;
    uint8_t numBlocks;
    uint8_t tempHash[HASH_LENGTH];
    
    //check arguments
    if(len == 0){
    // pl_log_e( TAG," hashDataB len == 0.\n");
        return FAIL;	    
    }
    if(hash == NULL){
        // pl_log_e( TAG," hashDataB NULL hash.\n");
        // return FAIL;	    
    }
    
    //get hash key
    // if((status = call c_keydistrib.getHashKeyB( &m_key1))!= SUCCESS){
    //     pl_log_e( TAG," hashDataB key not retrieved.\n");
    //     return status;
    // }

    // TODO BUG!? original getHashKey() (KeyDistribP.nc:164) only zeroes out the memory
    // it also marks it as bug
    memset(m_key, 0, RC2_KEY_SIZE);

    numBlocks = len / HASH_LENGTH;
    // pl_log_d( TAG," numBlocks == %d.\n", numBlocks);

    for(i = 0; i < numBlocks + 1; i++) {
    //incomplete block check, if input is in buffer, than copy data to input, otherwise use zeros as padding 
        for (j = 0; j < HASH_LENGTH; j++){
            if ((i * HASH_LENGTH + j) < len) {
                hash[j] = buffer[offset + i * HASH_LENGTH + j];
            } else {
                hash[j] = 0;
            } 
        }
        // if((status = call c_cryptoraw.hashDataBlockB(hash, 0, m_key1, tempHash)) != SUCCESS){
        //     pl_log_e( TAG," hashDataB calculation failed.\n"); 
        //     return status;
        // }
        if((status = hashDataBlockB(hash, 0, m_key, tempHash)) != SUCCESS){
            // pl_log_e( TAG," hashDataB calculation failed.\n"); 
            return status;
        }

        //copy result to key value for next round
        for(j = 0; j < HASH_LENGTH; j++){
            // m_key1->keyValue[j] = tempHash[j];
            m_key[j] = tempHash[j];
        }
    }
    //put hash to output
    for(j = 0; j < HASH_LENGTH; j++){
            hash[j] = tempHash[j];
    }
    return status;
}


bool RC2hash::hash(const uint8_t * const input, const uint16_t input_size, uint8_t *output, const uint16_t output_buffer_size)
{
    if(!input || !output){
        return false;
    }

    if(input_size < 1 || output_buffer_size < RC2_HASH_SIZE){
        return false;
    }

    if(hashDataB(input, 0, input_size, output) != SUCCESS){
        return false;
    }

// #ifdef DEBUG
//     std::cout << input_size << " " << output_buffer_size << " " << std::endl;
//     printBufferHex(input, input_size);
//     printBufferHex(output, output_buffer_size);
// #endif

    return true;
}

uint8_t RC2hash::hashSize()
{
	return RC2_HASH_SIZE;
}


RC2MAC::RC2MAC(RC2 *rc2): m_rc2(rc2), m_exp(expanded_key)
{
    memset(expanded_key, 0, RC2_KEY_SIZE);
}


uint8_t RC2MAC::macBuffer(const uint8_t* key, const uint8_t* buffer, uint8_t offset, uint8_t* pLen, uint8_t* mac)
{
    uint8_t i;
    uint8_t j;
    uint8_t xor_[BLOCK_SIZE];
    uint8_t status = SUCCESS;
    
    m_rc2->keyExpansion( m_exp, key);
        
    //if pLen is < BLOCK_SIZE then copy just pLen otherwise copy first block of data
    memset(xor_, 0, BLOCK_SIZE);
    if(*pLen < BLOCK_SIZE){
        memcpy(xor_, buffer + offset, *pLen);
    } else {
        memcpy(xor_, buffer + offset, BLOCK_SIZE);
    }
    //process buffer by blocks
    for (i = 0; i < (*pLen / BLOCK_SIZE) + 1; i++){
        m_rc2->encrypt (xor_, m_exp, xor_);
        for (j = 0; j < BLOCK_SIZE; j++){
            if ((*pLen <= (i * BLOCK_SIZE + j))){
                break;
            }
            xor_[j] = buffer[offset + i * BLOCK_SIZE + j] ^ xor_[j];
        }
    }
    //output mac
    memcpy(mac, xor_, BLOCK_SIZE);        
    
    return status;
}


bool RC2MAC::computeMAC(const uint8_t *key, const uint8_t key_size, const uint8_t *input, const uint16_t input_size, uint8_t *output, const uint16_t output_buffer_size)
{
    if(!key || !input || !output){
#ifdef __linux__
    std::cout << "NULL argument in computeMAC()" << std::endl;
#endif
        return false;   // NULL arguments
    }

    if(key_size != RC2_KEY_SIZE || input_size < 1 || output_buffer_size < RC2_MAC_SIZE){
#ifdef __linux__
    std::cout << "invalid argument in computeMAC()" << std::endl;
    std::cout << (int) key_size << " " << input_size << " " << output_buffer_size << std::endl;
#endif
        return false;   // invalid argument
    }

    if(input_size > 255){
#ifdef __linux__
    std::cout << "input size too big argument in computeMAC()" << std::endl;
#endif
        return false;   // does not fit into uint8_t
    }

    uint8_t in_size = input_size;


    if(macBuffer(key, input, 0, &in_size, output) != SUCCESS){
#ifdef __linux__
    std::cout << "macBuffer() failed" << std::endl;
#endif
        return false;
    }
    return true;
}

uint8_t RC2MAC::keySize()
{
	return RC2_KEY_SIZE;
}

uint8_t RC2MAC::macSize()
{
	return RC2_HASH_SIZE;
}
