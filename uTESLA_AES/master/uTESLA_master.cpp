#include <iostream>
#include <chrono>
#include <thread>

#include "uTESLAMaster.h"
#include "AES_crypto.h"
//#include "AES.h"

#define DEFAULT_PORT    "/dev/ttyUSB0"

using namespace std;

const uint8_t initial_key[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                0, 0, 0, 0, 0, 0, 0, 0 };

const uint8_t data[5][10] = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
                            {3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
                            {4, 4, 4, 4, 4, 4, 4, 4, 4, 4}};

int main(int argc, char **argv)
{
    string serial_port;

//    Blake224 hash;
//    BlakeHMAC hmac;
    AES aes;
    AEShash hash(&aes);
    AESMAC hmac(&aes);

    if(argc < 2){
        serial_port = DEFAULT_PORT;
    } else {
        serial_port = argv[1];
    }
    
    cout << "Using serial port: " << serial_port << endl;

    // (std::string serial_port, const uint8_t *initial_key, const uint32_t rounds_num, Hash *hash, MAC *mac)
    uTeslaMaster *utesla = NULL;

    try {
        utesla = new uTeslaMaster(serial_port, initial_key, 4, &hash, &hmac);
    } catch(uTeslaMasterException ex){
        cerr << ex.what() << endl;
        return -1;
    }

    utesla->printLastElementHex();

    // utesla->
    // bool broadcastMessage(const uint8_t* data, const uint16_t data_len);

    for(int i=0;i<3;i++){
        utesla->broadcastMessage(data[i], 10);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        utesla->newRound();
        std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    }

    cout << "Done." << endl;

    delete utesla;

    return 0;
}
