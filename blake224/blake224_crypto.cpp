#include "blake224_crypto.h"


bool Blake224::hash(const uint8_t * const input, const uint16_t input_size, uint8_t *output, const uint16_t output_buffer_size)
{
	if(output_buffer_size < hashSize()){
		return false;
	}

    // memset(output, 0, output_buffer_size);  // TODO remove
	blake224_hash(output, input, input_size);

	return true;
}

uint8_t Blake224::hashSize()
{
	return BLAKE_HASH_SIZE;
}

bool BlakeHMAC::computeMAC(const uint8_t *key, const uint8_t key_size, const uint8_t *input, const uint16_t input_size, uint8_t *output, const uint16_t output_buffer_size)
{
	uint8_t	    pad[BLAKE_BLOCK_SIZE];
	uint8_t     padded_key[BLAKE_BLOCK_SIZE];

    // std::cout << "Using key " << std::endl;
    // printBufferHex(key, key_size);
    // std::cout << "to compute MAC for " << std::endl;
    // printBufferHex(input, input_size);

    if(output_buffer_size < BLAKE_HASH_SIZE){
        return false;
    }

	if(key_size > BLAKE_BLOCK_SIZE){
        blake224_hash(padded_key, key, key_size);
    } else if(key_size < BLAKE_BLOCK_SIZE){
        memset(padded_key, 0, BLAKE_BLOCK_SIZE);
        memcpy(padded_key, key, key_size);
    } else {
        memcpy(padded_key, key, BLAKE_BLOCK_SIZE);
    }

    // pad is now ipad
    for(int i=0;i<BLAKE_BLOCK_SIZE;i++){
        pad[i] = 0x36 ^ padded_key[i];
    }

    blake224_init(&m_blake_state);

    // hashing ipad
    blake224_update(&m_blake_state, pad, BLAKE_BLOCK_SIZE);

    // reusing pad for opad, ipad is not used from now on
    for(int i=0;i<BLAKE_BLOCK_SIZE;i++){
        pad[i] = 0x5C ^ padded_key[i];
    }

    // hashing message
    blake224_update(&m_blake_state, input, input_size);

    // reusing padded_key for output for intermediate state to save some memory
    // hash size is smaller than block size so it will not overflow
    blake224_final(&m_blake_state, padded_key);

    // initializing new hash
    blake224_init(&m_blake_state);

    // hash opad
    blake224_update(&m_blake_state, pad, BLAKE_BLOCK_SIZE);

    // hash output from previous hashing
    blake224_update(&m_blake_state, padded_key, BLAKE_HASH_SIZE);
    
    // finish
    blake224_final(&m_blake_state, output);

    return true;
}

uint8_t BlakeHMAC::keySize()
{
	return BLAKE_BLOCK_SIZE;
}

uint8_t BlakeHMAC::macSize()
{
	return BLAKE_HASH_SIZE;
}
