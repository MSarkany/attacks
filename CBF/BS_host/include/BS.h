#ifndef HOST_H
#define HOST_H

#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdint.h>

#include "RC2_crypto.h"
#include <uTESLAMaster.h>

#define BUFF_SIZE 2048

using namespace std;


class BS {
private:
    RC2             m_rc2;
    RC2hash         m_hash;
    RC2MAC          m_mac;
    uTeslaMaster    *m_utesla   = NULL;
    string          m_dev_path;         // path to device
    int             m_dev_fd    = 0;    // device file descriptor
    int64_t        m_duration;         // duration of the attack

    // reads line from device
    int read_line(char *buffer, int max_len);

public:
    // constructor
    BS(const string dev_path, const uint64_t duration, const uint8_t *utesla_initial_key, const uint16_t rounds_num);
    // destructor
    virtual ~BS();
    // function with main loop - handling all the work
    bool runScenario();
};


// exception for host constructor
class BSException : public exception {
    string m_message;
public:
    BSException(string message)
    {
        m_message = message;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
};

#endif // HOST_H
