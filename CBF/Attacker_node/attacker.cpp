#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "../../uTESLA_AES/uTESLA.h"

#define CBF_DURATION_ATTACKER   30000
#define MSG_BUFF_SIZE           70
#define RELAY_BS_ID             1

uint8_t node_id     = RELAY_BS_ID;

uint8_t message_len = 0;
uint8_t message_hdr = 0;
uint8_t message_buffer[MSG_BUFF_SIZE];
uint8_t send_mode   = 0;

#ifdef RF12_COMPAT
#define RF12_HDR_SIZE   4
#define RF12_HDR_INDEX  3
#else
#define RF12_HDR_SIZE   3
#define RF12_HDR_INDEX  1
#endif // RF12_COMPAT


void setup () {
    // initialization
    Serial.begin(BAUD_RATE);

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
    
    Serial.setTimeout(300); // TODO ?


    // Serial.flush(); 
    uint32_t end = millis() + CBF_DURATION_ATTACKER;
    uint32_t now = 0;

    while((now = millis()) < end){
        if(Serial.available() > 0){
            memset(message_buffer, 0, MSG_BUFF_SIZE);
            message_len = Serial.readBytes((char*) message_buffer, MSG_BUF_SIZE);
            // Serial.println(message_len);
            if(message_len > RF12_HDR_SIZE){
                node_id = message_buffer[RF12_HDR_INDEX] & RF12_HDR_MASK;
                // actual payload length
                if(message_buffer[RF12_HDR_SIZE] == MSG_TYPE_DATA){
                // there may be 2 messages
                // sending only the first message
                    message_len = message_buffer[2]; // TODO define for 2 (RF12_SIZE_OFFSET) // TODO also rename INDEX to OFFSET
                } else {
                    message_len -= RF12_HDR_SIZE;
                }

                send_mode = (message_buffer[RF12_HDR_INDEX] & RF12_HDR_DST) ? MODE_DST : MODE_SRC;
                // Serial.println(send_mode, DEC);
                
                rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);
                message_hdr = createHeader(node_id, send_mode, 0);
                // delay(100);
                rf12_sendNow(message_hdr, message_buffer + RF12_HDR_SIZE, message_len);
            }
        }
        
        if(rf12_recvDone() && !rf12_crc){
            if((rf12_hdr & RF12_HDR_MASK) != RELAY_BS_ID){
                rf12_recvDone();
                continue;
            }
            message_len = rf12_len + RF12_HDR_SIZE;
            memcpy(message_buffer, rf12_buf, message_len);
            replyAck();
            rf12_recvDone();
            Serial.write(message_buffer, message_len);
        }
    }
}


// using infinite loops inside the loop so conditions don't have to be checked all the time
void loop ()
{
    // end of the scenario - sleeping forever    
    Serial.println("End of scenario");
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable(); 
    sleep_mode();
}
