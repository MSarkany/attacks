#include <Arduino.h>
#include <EEPROM.h>
#include "RF12.h"
#include <avr/sleep.h>

#include "RC2_crypto.h"
#include "../cbf.h"
#include "../../uTESLA_AES/client/uTESLAClient.h"
#include "../../common.h"


#define MESSAGE_BUFF_NUM    2      // buffer for 2 messages
#define CLIENT_DURATION     20000   // 10 seconds

#define BUFFER_SIZE 70

typedef struct {
    int8_t round_num;
    uint8_t len;
    uint8_t buffer[40];
} message_t;

static uint16_t node_id             = 0;
static uint16_t parent_id           = 0;
static uint8_t  header              = 0;

static bool     is_BS               = false;          // set to true on BS

static uint8_t  rcvd_buff[MSG_BUF_SIZE];
static uint8_t  rcvd_len            = 0;
static uint8_t  rcvd_hdr            = 0;
uint8_t         last_hash[] = { 0x5E, 0xC2, 0x29, 0x0B, 0x0F, 0xB7, 0xCC, 0xA7 };

static RC2              rc2;
static RC2MAC           hmac(&rc2);
static RC2hash          hash(&rc2);
static uTeslaClient     utesla_client(last_hash, &hash, &hmac);

message_t       messages[MESSAGE_BUFF_NUM];

static void initMessages()
{
    memset(messages, -1, MESSAGE_BUFF_NUM * sizeof(message_t));
}

static bool addMessage(const uint8_t *message, const uint8_t message_size)
{
    for(int i=0;i<=MESSAGE_BUFF_NUM;i++){
        if(messages[i].round_num <= utesla_client.getRoundNum()){
            messages[i].round_num = utesla_client.getRoundNum() + 1;
            messages[i].len = message_size;
            memcpy(messages[i].buffer, message, message_size);
            return true;
        }
    }

    return false;
}

static void verifyMessages()
{
    for(int i=0;i<MESSAGE_BUFF_NUM;i++){
        // Serial.println(i, DEC);
        if(messages[i].round_num != utesla_client.getRoundNum()){
            continue;
        }
        // Serial.println(messages[i].round_num, DEC);
        uint8_t message_len = messages[i].len - utesla_client.getMacSize();
        Serial.print("Msg ");
        printBuffer(messages[i].buffer, message_len);
        if(utesla_client.verifyMAC(messages[i].buffer, message_len, messages[i].buffer + message_len)){
            Serial.println("OK");
            Serial.flush();
        } else {
            Serial.println("NOK");
            Serial.flush();
        }
    }
}

void setup()
{
    Serial.begin(BAUD_RATE);
    node_id = EEPROM.read(NODE_ID_LOCATION);

    rf12_initialize(node_id, RADIO_FREQ, RADIO_GROUP);

    // BS has equal own and parrent IDs
    if(node_id == 1){
        is_BS = true;
    }

    if(is_BS){
        Serial.println("\nslave\n");
        header = createHeader(node_id, MODE_SRC, 0);
    } else {
        Serial.println("\nclient\n");
    }

    Serial.flush();
    
    initMessages();

    if(!is_BS){
        randomSeed(analogRead(0));
    }
}

void loop()
{
    if(is_BS){
        char buffer[BUFFER_SIZE];
        if(Serial.available() > 0){
            uint8_t len1 = Serial.read();
            while(Serial.available() < 1);
            uint8_t len2 = Serial.read();
            if(len1 != len2){
                Serial.print("Incrct len: ");
                Serial.println(len2);
                Serial.flush();
                return;
            }

            while(Serial.available() < 1);
            if(Serial.readBytes(buffer, len1) != len1){
                Serial.println("read()");
                Serial.flush();
                return;
            }

            Serial.println("Snd: ");
            printBuffer(buffer, len1);
            Serial.flush();

            rf12_sendNow(header, buffer, len1);
        }
    } else {
        uint32_t loop_start = millis();

        while(waitReceive(loop_start + CLIENT_DURATION)){
            copy_rf12_to_buffer;
            Serial.print("Rcvd: ");
            printBuffer(rcvd_buff, rcvd_len);
            if(rcvd_buff[0] == MSG_TYPE_DATA){
                // Serial.print("Msg add ");
                if(!addMessage(rcvd_buff + 1, rcvd_len - 1)){
                    printError(ERR_MSG_ADD);
                }
                Serial.flush();
            } else if(rcvd_buff[0] == MSG_TYPE_KEY){
                // check message length
                if(rcvd_len - 1 < utesla_client.getKeySize()){
                    // Serial.println(err_msg);
                    // Serial.flush();
                    printError(ERR_KEY_UPDT);
                    continue;
                }
                // update key
                // Serial.print("Key update ");
                if(!utesla_client.updateKey(rcvd_buff + 1)){
                    printError(ERR_KEY_UPDT);
                }
                Serial.flush();
                // verify messages from previous round
                verifyMessages();
                Serial.println("done vrfng");
                Serial.flush();
            } else {
                // Serial.println(err_msg);
                // Serial.flush();
                printError(ERR_INVARG);
            }
        }

        Serial.println("Finished");
        Serial.flush();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable(); 
        sleep_mode();
    }
}
