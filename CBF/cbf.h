#ifndef _RELAY_H_
#define _RELAY_H_

#include "../common.h"

// phases' durations
#define RP_DURATION_LEGIT       20000
#define RP_DURATION_ATTACKER    20000
#define MXP_DURATION_LEGIT      25000
#define MXP_DURATION_ATTACKER   30000
#define CBF_DURATION_ATTACKER RP_DURATION_ATTACKER + MXP_DURATION_ATTACKER

#endif // _RELAY_H_
