#ifndef HOST_H
#define HOST_H

#include <iostream>
#include <string>
#include <exception>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#ifndef LINUX_ONLY
#define LINUX_ONLY
#endif
#include "uTESLA.h"

#define BUFF_SIZE       2048
#define RECEIVE_PORT    33333
#define SEND_PORT       3333

using namespace std;

// data used to process the packet
struct AttackerData {

};

// generic pc host for CBF attack
class AttackerHost {
private:
    string          m_dev_path; // path to device
    int             m_dev_fd;   // device file descriptor
    int64_t         m_duration; // duration of the attack
    AttackerData    m_data;     // structure containing data needed in processPacket()

    bool processPacket(uint8_t *buffer, int *size, AttackerData &data);

public:
    // constructor
    AttackerHost(const string &dev_path, const int64_t duration);

    // destructor
    virtual ~AttackerHost();
    // function with main loop - handling all the work
    virtual int runScenario();
};

// exception for host constructor
class HostException : public exception {
    string m_message;
public:
    HostException(string message)
    {
        m_message = message;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
};

#endif // HOST_H
