// TODO add signal for duration + signal handler for SIGINT etc.

#include <getopt.h>

#include "host.h"

using namespace std;

void print_usage(const char *app_name)
{
    cout << app_name
         << " -p [ -d ]" << endl
         << "    -d    duration" << endl
         << "    -p    JeeLink device path" << endl;
}

// negative values are errors, positive are converted values
long long string_to_unsigned(const char *str)
{
    if(!str){
        return -1;
    }

    if(str[0] > '9' || str[0] < '0'){
        return -2;
    }

    return atoi(str);
}

int main(int argc, char **argv)
{
    string      dev_path;
    long long   duration;
    char        opt;

    while((opt = getopt(argc, argv, "d:p:")) != -1)
    {
        switch(opt)
        {
        case 'd':
            duration = string_to_unsigned(optarg);
            break;
        case 'p':
            dev_path = optarg;
            break;
        default:
            print_usage(argv[0]);
            break;
        }
    }

    if(duration < 1){
        cerr << "Duration not set or invalid" << endl;
        return -5;
    }

    if(dev_path.empty()){
        cerr << "Device path not set" << endl;
        return -6;
    }

    // create host
    AttackerHost *host;
    try{
        host = new AttackerHost(dev_path, duration);
    } catch (HostException &ex){
        cerr << ex.what() << endl;
        return -1;
    }

    // main loop
    host->runScenario();

    delete host;

    return 0;
}
